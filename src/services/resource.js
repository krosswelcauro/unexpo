import config from './config';
const $ = window.$;



export const index = async (route, token, callback) => {
  let { headers } = config;
  let headers2 = { Authorization: token };

  const myHeaders = Object.assign(headers, headers2);
  console.log(config.api +route,token);
  
  await new Promise((resolve, reject) => {
    fetch(config.api + route, {
      method: 'GET',
      headers: myHeaders,
    }
    ).then(function (response) {
      if (response.ok) {
        console.log(response)
        resolve(response.json())

      } else {
        /* Manejar errores del cliente*/
        reject(new Error('Ha ocurrido un error'))
      }
    }).catch(function (e) {
      reject(e)
    })
  }).then(response => {
    callback(response)
  }).catch(e => {
    callback(e)
  });
}

export const indexWith= async (route, token,param,callback) => {
  let { headers } = config;
  let headers2 = { Authorization: token };
  console.log(token)
  const myHeaders = Object.assign(headers, headers2);
  await new Promise((resolve, reject) => {
    fetch(config.api + route, {
      method: 'GET',
      headers: myHeaders,
      body:JSON.stringify(param)
    }
    ).then(function (response) {
      if (response.ok) {
        console.log(response)
        resolve(response.json())

      } else {
        /* Manejar errores del cliente*/
        reject(new Error('Ha ocurrido un error'))
      }
    }).catch(function (e) {
      reject(e)
    })
  }).then(response => {
    callback(response)
  }).catch(e => {
    callback(e)
  });
}







export const update = async (route, token, find,body,callback) => {
  let users;
  let { headers } = config;
  let headers2 = { Authorization: token };
  const myHeaders = Object.assign(headers, headers2);



  var requestOptions = {
    method: 'PUT',
    body: JSON.stringify(body),
    headers: myHeaders,
    redirect: 'follow'
  };

  await new Promise((resolve, reject) => {
    fetch(config.api +`${route}/${find}`, requestOptions
    ).then(function (response) {
      if (response.ok) {
        console.log(response)
        resolve(response.json())

      } else {
        reject(new Error('Ha ocurrido un error'))
      }
    }).catch(function (e) {
      reject(e)
    })
  }).then(response => {
    callback(response)
  }).catch(e => {
    callback(e)
  });
}


export const updateWith = async (route, token, find, body,callback) => {
  let users;
  let { headers } = config;
  let headers2 = { Authorization: token };
  const myHeaders = Object.assign(headers, headers2);

  var raw = JSON.stringify(body);

  var requestOptions = {
    method: 'PUT',
    body: raw,
    headers: myHeaders,
    redirect: 'follow'
  };

  await new Promise((resolve, reject) => {
    fetch(config.api + route + JSON.stringify(find), requestOptions
    ).then(function (response) {
      if (response.ok) {
        console.log(response)
        resolve(response.json())

      } else {
        reject(new Error('Ha ocurrido un error'))
      }
    }).catch(function (e) {
      reject(e)
    })
  }).then(response => {
    callback(response)
  }).catch(e => {
    callback(e)
  });
}



export const show = async (route, token, find,callback) => {
  let users;
  let { headers } = config;
  let headers2 = { Authorization: token };
  const myH = Object.assign(headers, headers2);

  var requestOptions = {
    method: "GET",
    headers: myH,
    redirect: 'follow'
  };
  
  
  

  await new Promise((resolve, reject) => {
    console.log(config.api +`${route}/${find}`,requestOptions)
    fetch(config.api +`${route}/${find}`, requestOptions
    ).then(function (response) {
      if (response.ok) {
        console.log(response)
        resolve(response.json())

      } else {
        reject(new Error('Ha ocurrido un error'))
      }
    }).catch(function (e) {
      reject(e)
    })
  }).then(response => {
    callback(response)
  }).catch(e => {
    callback(e)
  });
}

export const showWith = async (route, token, find,body,callback) => {
  let users;
  let { headers } = config;
  let headers2 = { Authorization: token };
  const myH = Object.assign(headers, headers2);

  var requestOptions = {
    method: "GET",
    headers: myH,
    body:body,
    redirect: 'follow'
  };
  await new Promise((resolve, reject) => {
    console.log(config.api +`${route}${find}`,requestOptions)
    fetch(config.api +`${route}${find}`, requestOptions
    ).then(function (response) {
      if (response.ok) {
       
        return response.json()

      } else {
        reject(new Error('Ha ocurrido un error'))
      }
    }).catch(function (e) {
      reject(e)
    })
  }).then(response => {
    callback(response)
  }).catch(e => {
    callback(e)
  });
}
export const Assing = async (route, token, find,body,callback) => {
  let users;
  let { headers } = config;
  let headers2 = { Authorization: token };
  const myH = Object.assign(headers, headers2);

  var requestOptions = {
    method: "PUT",
    headers: myH,
    body:body,
    redirect: 'follow'
  };
  await new Promise((resolve, reject) => {
    console.log(config.api +`${route}${find}/assign`,requestOptions)
    fetch(config.api +`${route}${find}/assign`, requestOptions
    ).then(function (response) {
      console.log(response)
      if (response.ok) {
       
        return response.json()

      } else {
        reject(new Error('Ha ocurrido un error'))
      }
    }).catch(function (e) {
      reject(e)
    })
  }).then(response => {
    callback(response)
  }).catch(e => {
    callback(e)
  });
}

export const store = async (route, token,body,callback) => {
  let users;
  let { headers } = config;
  let headers2 = { Authorization: token };
  const myHeaders = Object.assign(headers, headers2);

  var requestOptions = {
    method: 'POST',
    headers: myHeaders,
    body:JSON.stringify(body),
    redirect: 'follow'
  };
  
  console.log(requestOptions,config.api + route )
  await new Promise((resolve, reject) => {
    fetch(config.api + route , requestOptions
    ).then(function (response) {
      if (response.ok) {
        console.log(response)
        resolve(response.json())

      } else {
        reject(new Error('Ha ocurrido un error'))
      }
    }).catch(function (e) {
      reject(e)
    })
  }).then(response => {
    callback(response)
  }).catch(e => {
    callback(e)
  });
}

export const destroy = async (route, token,find,callback) => {

  let { headers } = config;
  let headers2 = { Authorization: token };
  const myHeaders = Object.assign(headers, headers2);

  var requestOptions = {
    method: 'DELETE',
    headers: myHeaders,
    redirect: 'follow'

  };

  await new Promise((resolve, reject) => {
    fetch(config.api  +`${route}/${find}`, requestOptions
    ).then(function (response) {
      if (response.ok) {
        console.log(response)
        resolve(response.json())

      } else {
        reject(new Error('Ha ocurrido un error'))
      }
    }).catch(function (e) {
      reject(e)
    })
  }).then(response => {
    callback(response)
  }).catch(e => {
    callback(e)
  });
}

export const restore = async (route, token,callback) => {

  let { headers } = config;
  let headers2 = { Authorization: token };
  const myHeaders = Object.assign(headers, headers2);

  var requestOptions = {
    method: 'POST',
    headers: myHeaders,
    redirect: 'follow'

  };

  await new Promise((resolve, reject) => {
    fetch(config.api  +route, requestOptions
    ).then(function (response) {
      if (response.ok) {
        console.log(response)
        resolve(response.json())

      } else {
        reject(new Error('Ha ocurrido un error'))
      }
    }).catch(function (e) {
      reject(e)
    })
  }).then(response => {
    callback(response)
  }).catch(e => {
    callback(e)
  });
}

export const destroyAll = async (route, token,array,callback) => {

  let { headers } = config;
  let headers2 = { Authorization: token };
  const myHeaders = Object.assign(headers, headers2);

  var requestOptions = {
    method: 'POST',
    headers: myHeaders,
    body:JSON.stringify(array)

  };
console.log(config.api  +route,requestOptions)
  await new Promise((resolve, reject) => {
    fetch(config.api  +route, requestOptions
    ).then(function (response) {
      if (response.ok) {
        console.log(response)
        resolve(response.json())

      } else {
        console.log(response)
      }
    }).catch(function (e) {
      reject(e)
    })
  }).then(response => {
    callback(response)
  }).catch(e => {
    callback(e)
  });
}
