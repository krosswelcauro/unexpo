import config from './config';
const $ = window.$;



export const getGeneral = async (route, token, callback) => {
  let { headers } = config;
  let headers2 = { Authorization: token };
  const myHeaders = Object.assign(headers, headers2);
  await new Promise((resolve, reject) => {
    fetch(config.api + route, {
      method: 'GET',
      headers: myHeaders,
     

    }
    ).then(function (response) {
      if (response.ok) {
        resolve(response.json())

      } else {
        reject(new Error('Ha ocurrido un error'))
      }
    }).catch(function (e) {
      reject(e)
    })
  }).then(response => {
    callback(response)
  }).catch(e => {
    callback(e)
  });
}

export const getAll = async (route, token, callback) => {
  let aller;
  let { headers } = config;
  let headers2 = { Authorization: token };
  const myHeaders = Object.assign(headers, headers2);
console.log(myHeaders,config.api+route+"5&page=1")
  await new Promise((resolve, reject) => {
    fetch(config.api+route+"5&page=1", {
      method: 'GET',
      headers: myHeaders,
      redirect: 'follow',

    }
    ).then(function (response) {
      console.log(response)
    

      if (response.ok) {
        resolve(response.json())

      } else {
        reject(new Error('Ha ocurrido un error'))
      }
    }).catch(function (e) {
      reject(e)
    })
  }).then(response => {

    callback(response)
  }).catch(e => {
    callback(e)
  });

  return aller;
}

export const getAllI = async (route, token, callback) => {
  let aller;
  let { headers } = config;
  let headers2 = { Authorization: token };
  const myHeaders = Object.assign(headers, headers2);
console.log(myHeaders)
  await new Promise((resolve, reject) => {
    fetch(config.api+route+"?page=1&limit=5", {
      method: 'GET',
      headers: myHeaders,
      redirect: 'follow',

    }
    ).then(function (response) {
      console.log(response)
     

      if (response.ok) {
        console.log(response)
        resolve(response.json())

      } else {
        reject(new Error('Ha ocurrido un error'))
      }
    }).catch(function (e) {
      reject(e)
    })
  }).then(response => {

    callback(response)
  }).catch(e => {
    callback(e)
  });

  return aller;
}



//
export const getAllWith = async (route, token,q,page,status,callback) => {
  let users;
  let { headers } = config;
  let headers2 = { Authorization: token };
  const myHeaders = Object.assign(headers, headers2);

  await new Promise((resolve, reject) => {
    fetch(config.api+route+q+"&page="+page, {
      method: 'GET',
      headers: myHeaders,
      redirect: 'follow',

    }
    ).then(function (response) {
      if (response.ok) {
       
        resolve(response.json())

      } else {
        reject(new Error('Ha ocurrido un error'))
      }
    }).catch(function (e) {
      reject(e)
    })
  }).then(response => {
    callback(response)
  }).catch(e => {
    callback(e)
  });

  return users;
}



export const update = async (route, token, find, status,callback) => {
  let users;
  let { headers } = config;
  let headers2 = { Authorization: token };
  const myHeaders = Object.assign(headers, headers2);

  var raw = JSON.stringify(status);

  var requestOptions = {
    method: 'PUT',
    body: raw,
    headers: myHeaders,
    redirect: 'follow'
  };

  await new Promise((resolve, reject) => {
    fetch(config.api + route + JSON.stringify(find), requestOptions
    ).then(function (response) {
      if (response.ok) {
        console.log(response)
        resolve(response.json())

      } else {
        reject(new Error('Ha ocurrido un error'))
      }
    }).catch(function (e) {
      reject(e)
    })
  }).then(response => {
    callback(response)
  }).catch(e => {
    callback(e)
  });
}


export const updateWith = async (route, token, find, body,callback) => {
  let users;
  let { headers } = config;
  let headers2 = { Authorization: token };
  const myHeaders = Object.assign(headers, headers2);

  var raw = JSON.stringify(body);

  var requestOptions = {
    method: 'PUT',
    body: raw,
    headers: myHeaders,
    redirect: 'follow'
  };

  await new Promise((resolve, reject) => {
    fetch(config.api + route + JSON.stringify(find), requestOptions
    ).then(function (response) {
      if (response.ok) {
        console.log(response)
        resolve(response.json())

      } else {
        reject(new Error('Ha ocurrido un error'))
      }
    }).catch(function (e) {
      reject(e)
    })
  }).then(response => {
    callback(response)
  }).catch(e => {
    callback(e)
  });
}



export const findOrChange = async (method,route, token, find,callback) => {
  let users;
  let { headers } = config;
  let headers2 = { Authorization: token };
  const myH = Object.assign(headers, headers2);

  var requestOptions = {
    method: method,
    headers: myH,
    redirect: 'follow'
  };
  
  
  console.log(requestOptions)


  await new Promise((resolve, reject) => {
    console.log(config.api +`${route}${find}`,requestOptions)
    fetch(config.api +`${route}${find}`, requestOptions
    ).then(function (response) {
      if (response.ok) {
        console.log(response)
        resolve(response.json())

      } else {
        console.log(response)
        reject(new Error('Ha ocurrido un error'))
      }
    }).catch(function (e) {
      reject(e)
    })
  }).then(response => {
    callback(response)
  }).catch(e => {
    callback(e)
  });
}


export const changeStatus = async (method,route, token, find,callback) => {
  let users;
  let { headers } = config;
  let headers2 = { Authorization: token };
  const myH = Object.assign(headers, headers2);

  var requestOptions = {
    method: "PUT",
    headers: myH,
    redirect: 'follow'
  };
  

  
  console.log(requestOptions,config.api +`${route}${method}/${find}`,find)

///${find}/${method}
  await new Promise((resolve, reject) => {
    fetch(config.api +`${route}${method}/${find}`, requestOptions
    ).then(function (response) {
      if (response.ok) {
        console.log(response)
        resolve(response.json())

      } else {
        console.log(response)
        reject(new Error('Ha ocurrido un error'))
      }
    }).catch(function (e) {
      reject(e)
    })
  }).then(response => {
    callback(response)
  }).catch(e => {
    callback(e)
  });
}


export const findOrChangeWith = async (method,route, token, find,body,callback) => {
  let users;
  let { headers } = config;
  let headers2 = { Authorization: token };
  const myH = Object.assign(headers, headers2);
  let requestOptions = {
    method: method,
    headers: myH,
    body:body,
    redirect: 'follow'
  };
 
  await new Promise((resolve, reject) => {
    console.log(config.api +`${route}${find}`,requestOptions)
    fetch(config.api +`${route}${find}`, requestOptions
    ).then(function (response) {
      if (response.ok) {
      
        resolve(response.json())

      } else {
        reject(new Error('Ha ocurrido un error'))
      }
    }).catch(function (e) {
      reject(e)
    })
  }).then(response => {
    callback(response)
  }).catch(e => {
    callback(e)
  });
}


export const Assing = async (route, token, find,body,callback) => {
  let users;
  let { headers } = config;
  let headers2 = { Authorization: token };
  const myH = Object.assign(headers, headers2);

  var requestOptions = {
    method: "PUT",
    headers: myH,
    body:body,
    redirect: 'follow'
  };
  await new Promise((resolve, reject) => {
    console.log(config.api +`${route}${find}/assign`,requestOptions)
    fetch(config.api +`${route}${find}/assign`, requestOptions
    ).then(function (response) {
      console.log(response)
      if (response.ok) {
       
        return response.json()

      } else {
        reject(new Error('Ha ocurrido un error'))
      }
    }).catch(function (e) {
      reject(e)
    })
  }).then(response => {
    callback(response)
  }).catch(e => {
    callback(e)
  });
}

export const Save = async (route, token,body,callback) => {
  let users;
  let { headers } = config;
  let headers2 = { Authorization: token };
  const myHeaders = Object.assign(headers, headers2);

  var requestOptions = {
    method: 'POST',
    headers: myHeaders,
    redirect: 'follow',
    body:body
  };

  await new Promise((resolve, reject) => {
    fetch(config.api + route , requestOptions
    ).then(function (response) {
      if (response.ok) {
        
        resolve(response.json())

      } else {
        console.log(response)
        reject(new Error('Ha ocurrido un error'))
      }
    }).catch(function (e) {
      reject(e)
    })
  }).then(response => {
    callback(response)
  }).catch(e => {
    callback(e)
  });
}

