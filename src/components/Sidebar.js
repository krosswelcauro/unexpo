import React, { Component, Fragment } from 'react';
import { BrowserRouter as Router, Route, Switch, Link } from 'react-router-dom';


 class Sidebar extends Component {
    render() {
        return (
            <Fragment>
                 <section className="full-box cover dashboard-sideBar show-sidebar">
		<div className="full-box dashboard-sideBar-bg btn-menu-dashboard"></div>
		<div className="full-box dashboard-sideBar-ct mCustomScrollbar _mCS_1 mCS-autoHide"><div id="mCSB_1" className="mCustomScrollBox mCS-light-thin mCSB_vertical mCSB_inside" style={{maxHeight: "none"}} tabindex="0"><div id="mCSB_1_container" className="mCSB_container" style={{position: "relative top: 0",left: 0}} dir="ltr">
		
			<div className="full-box text-uppercase text-center text-titles dashboard-sideBar-title">
				unexpo <i className="zmdi zmdi-close btn-menu-dashboard visible-xs"></i>
			</div>
	
			<div className="full-box dashboard-sideBar-UserInfo">
				<div className="full-box">
					<img src={require('./resources/img/active.png')} alt="UserIcon" className="mCS_img_loaded"/>
					<figcaption classNameName="text-center text-titles">User Name</figcaption>
				</div>
				<ul className="full-box list-unstyled text-center">
					<li>
						<a href="#!">
							<i className="zmdi zmdi-settings"></i>
						</a>
					</li>
					<li>
						<a href="#!" className="btn-exit-system">
							<i className="zmdi zmdi-power"></i>
						</a>
					</li>
				</ul>
			</div>
		
			<ul className="list-unstyled full-box dashboard-sideBar-Menu">
				<li>
					<Link to="/home">
						<i className="zmdi zmdi-view-dashboard zmdi-hc-fw"></i> Inicio
					</Link>
				</li>
				<li>
					<a href="#!" className="btn-sideBar-SubMenu">
						<i className="zmdi zmdi-case zmdi-hc-fw"></i> Administración <i className="zmdi zmdi-caret-down pull-right"></i>
					</a>
					<ul className="list-unstyled full-box">
						<li>
							<Link to="/carrera"><i className="zmdi zmdi-folder-person"></i> Carrera</Link>
						</li>
						<li>
							<Link to="/secciones"><i className="zmdi zmdi-graduation-cap zmdi-hc-fw"></i> Secciones</Link>
						</li>
						<li>
							<Link to="/asignatura"><i className="zmdi zmdi-book zmdi-hc-fw"></i> Asignaturas</Link>
						</li>
						<li>
							<Link to="/profesor"><i className="zmdi zmdi-male-alt zmdi-hc-fw"></i> Profesor</Link>
						</li>
						<li>
							<Link to="/alumno"><i className="zmdi zmdi-face zmdi-hc-fw"></i> Alumno</Link>
						</li>
					</ul>
				</li>
				<li>
					<a href="#!" className="btn-sideBar-SubMenu">
						<i className="zmdi zmdi-account-add zmdi-hc-fw"></i> Usuarios <i className="zmdi zmdi-caret-down pull-right"></i>
					</a>
					<ul className="list-unstyled full-box">
						<li>
							<Link to="/"><i className="zmdi zmdi-account zmdi-hc-fw"></i> Administrador</Link>
						</li>
						<li>
							<Link href="/"><i className="zmdi zmdi-male-alt zmdi-hc-fw"></i> Profesor</Link>
						</li>
						
					</ul>
				</li>
				
			</ul>
		</div>
        <div id="mCSB_1_scrollbar_vertical" className="mCSB_scrollTools mCSB_1_scrollbar mCS-light-thin mCSB_scrollTools_vertical" style={{display: "block"}}><a href="#" className="mCSB_buttonUp" style={{display:"block"}}></a><div className="mCSB_draggerContainer"><div id="mCSB_1_dragger_vertical" className="mCSB_dragger" style={{position: "absolute",minHeight: 30, display: "block", height: 58, maxHeight: 152, top: 0}}><div className="mCSB_dragger_bar" style={{lineHeight: 30}}></div></div>
        
        <div className="mCSB_draggerRail"></div></div><a href="#" className="mCSB_buttonDown" style={{display: "block"}}></a>
        
        </div>
        </div>
        </div>
	</section>
            </Fragment>
            
        )
    }
}

export default Sidebar;