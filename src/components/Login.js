import React, { Component } from 'react';
import {login} from './../services/auth';
import {m_toast} from './resources/sweetalert'
import Spinners from './Spinners'
import { BrowserRouter as Router, Route, Switch, Link ,Redirect} from 'react-router-dom';

class Login extends Component {
    state = { 
        errorEmail:false,
        errorPass:false,
        load:false

     }
     emailRef = React.createRef()
     passwordRef = React.createRef()
 
  Login = async()=>  {
  
   if(this.emailRef.current.value==""||this.passwordRef.current.value==""){
    this.setState({errorEmail:true})
            this.setState({errorPass:true}) 
            m_toast("error",'Existen campos vacios')
   }else{
    this.setState({load:true})
    await login("login",{email:this.emailRef.current.value,password:this.passwordRef.current.value,name:"Daniel"},(respuesta)=>{
        console.log(respuesta)
        if(respuesta.ok){
            console.log(respuesta.data.token)
            localStorage.setItem("token",respuesta.data.token)
            console.log(respuesta)
            m_toast("error",'Bienvenido')
            return this.props.history.push('/secciones');
            
        }
        this.setState({load:false})
      })

      
      //"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xOTIuMTY4LjEuNjo4MDAwXC9hcGlcL2xvZ2luIiwiaWF0IjoxNTg5NjQ1NjEyLCJleHAiOjE1ODk2NDkyMTIsIm5iZiI6MTU4OTY0NTYxMiwianRpIjoieWFYZGlhUFhUUVZ4eDB1MSIsInN1YiI6NCwicHJ2IjoiZjZiNzE1NDlkYjhjMmM0MmI3NTgyN2FhNDRmMDJiN2VlNTI5ZDI0ZCJ9.Zg8xh20d65sNHmmNO3nCoT2W3NfF7XHlsma9n_Z2y_M"
   }

  
  }

  test= ()=>{
    console.log(this.passwordRef.current.value,this.emailRef.current.value)
  }
  
    render() { 
        let email = (this.state.errorEmail) ? <span>Existe un error. Favor agregar email</span> : ""
        let pass = (this.state.errorPass) ? <span>Existe un error. Favor agregr la contraseña</span> : ""
        let load = (this.state.load) ? <Spinners /> : ""

        return (

            <div className="body cover">
                <div className="container">
                    <form className="full-box logInForm">
                        <p className="text-center text-muted"><i className="zmdi zmdi-account-circle zmdi-hc-5x"></i></p>
                        <p className="text-center text-muted text-uppercase">Inicia sesión con tu cuenta</p>
                        <div className="form-group label-floating">
                        <label className="control-label">Email</label>
                        <input className="form-control"  type="email" placeholder="Escribe tú E-mail"  ref ={this.emailRef}/>
                        {email}                        
                        </div>
                        <div className="form-group label-floating">
                        <label className="control-label" >Contraseña</label>
                        <input className="form-control"  type="password" placeholder="Escribe tú contraseña"  ref ={this.passwordRef} onKeyDown={()=>this.test()}/>
                        {pass}
                        </div>
                        <div className="form-group text-center">
                            <input type="button" value="Iniciar sesión" className="btn btn-raised btn-danger" onClick={()=>this.Login()}/>
                        </div>
                        {load}
                    </form>
                </div>
            </div>

        );
    }
}
 
export default Login;