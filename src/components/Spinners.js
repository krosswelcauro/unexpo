import React, { Component } from 'react';

class Spinners extends Component {
    state = {  }
    render() { 
        return ( 
            <div class="spinner">
            <div class="double-bounce1"></div>
            <div class="double-bounce2"></div>
          </div>
        );
    }
}
 
export default Spinners;