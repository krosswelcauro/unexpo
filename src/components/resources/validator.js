import * as moment from 'moment';

export const showCountry = (key) => {
    const country = this.props.paisResultado[key];
    const { id, name } = country;
    if (!id || !name) return null;
    return (
        <option value={id}> {name}</option>
    )
}

export const OnChangeEmail = (e) => {
    e.preventDefault();
    let regex = /[A-Z0-9._%+-]+@[A-Z0-9-]+.+.[A-Z]{2,4}/igm;
    let res = regex.test(e.target.value);
    if (res === false) {
        this.setState({
            email: true
        })
        return
    } else {
        this.setState({
            email: false
        })
        return
    }
}

export const onRadioChange = (e) => {
    if (e.target.value == 1) {
        this.setState({
            mostrar: true
        })
    } else {
        this.setState({
            mostrar: false
        })
    }
}

export const onRadioChangeGender = (e) => {
    this.setState({
        gender: e.target.value,
    })
    return
}

export const onPassword = (e) => {
    if (e.target.value !== '') {
        this.setState({
            password: e.target.value
        })
        return
    } else {
        this.setState({
            passwordMessage: true
        })
        return
    }
}

export const onConfirmPassword = (e) => {
    if (e.target.value !== '') {
        if (e.target.value !== this.state.password) {
            this.setState({
                confPassword: true
            })
            return
        } else {
            this.setState({
                confPassword: false
            })
            return
        }
    } else {
        this.setState({
            confPasswordError: true
        })
        return
    }
}

export const onInputFormmatPhone = (e) => {
    let regex = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
    let res = regex.test(e.target.value);

    if (res === false) {
        this.setState({
            phone: true
        })
        return
    } else {
        this.setState({
            phone: false
        })
        return
    }
}

export const onInputBirthday = (e) => {
    let date = e.target.value
    const NewDate = moment(date).format("DD/MM/YYYY");
    console.log(NewDate)
    this.setState({
        newFecha: NewDate
    })
    return
}