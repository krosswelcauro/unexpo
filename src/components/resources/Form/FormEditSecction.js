import React, { Component } from 'react';
import routes from './../../../services/routes';
import {updateCredentials,findUser} from './../../../services/user';
import {m_toast} from './../sweetalert';
import Swal from 'sweetalert2';

class FormEditUser extends Component {

    state = {
        Edit:true,
        userEdit:"",
        errorPass:false,
        errorUser:false,
        oldUsers:false}
    emailValue = React.createRef();
    oldPasswordValue = React.createRef();
    passwordValue = React.createRef();
    resetValue = React.createRef();
    saveValue = React.createRef();
    componentDidMount(){
        const  handle  = this.props.id
            
        let token = localStorage.getItem('token');
        let users =   findUser(routes.routeFindUser, token, Number(handle),respuesta => {
            if (respuesta instanceof Error) {
                console.log(respuesta)
            } else {
                console.log(respuesta)
                this.setState({oldUsers:respuesta})
              return respuesta;
            }
        }).then( )
    }
    Edit = (param) =>{
        let validation =true;
         if(this.oldPasswordValue.current.value == this.passwordValue.current.value){
            if(this.emailValue.current.value==""||this.passwordValue.current.value==""||this.oldPasswordValue.current.value==""){
                m_toast('error',"Algunos campos estan vacios no puede editar")
            
          }else{
            Swal.fire({
                title: "Advertencia",
                text: "¿Esta seguro de modificar la cuenta?",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Confirmar',
                cancelButtonText: "Cancelar"
            }).then(async (result) => {
                if (result.value) {
                    await  updateCredentials(routes.routeUpdateCredentials, 'Bearer' + " " + localStorage.getItem('token'),param,{"email":this.emailValue.current.value,"password":this.passwordValue.current.value},
                    respuesta => {
                        if (respuesta instanceof Error) {
              
                        } else {
                            console.log(respuesta)
                            //if condicion
                            this.setState({errorPass:false});
                           // callback(param,this.emailValue.current.value)
              
                        }
                    })
                    Swal.fire(
                        "Cambios realizados",
                        "Credenciales actualizadas",
                        'success'
                    );
                }
            })
            
           
        
        }
    
        }else{
           

            this.setState({errorPass:true});
         }
      
      }

    render() {
        console.log(this.state.oldUsers)
        let email ="example@gmail.com";
      if(this.state.oldUsers){
       email =  this.state.oldUsers.email
      }else{}
        console.log(this.state.oldUsers)
        let errorPass  = (this.state.errorPass)?<span className="form-text text-danger">Las contraseñas no coinciden</span>:<span className="form-text text-muted">Ingresa tu contraseña</span>

       // let send = (this.state.Edit)? <button type="button" className="btn btn-primary"  ref={this.saveValue} onClick={()=>this.Edit(this.props.userEdit,callback)}>Editar</button>
//:""
       // let errorMail = (this.state.errorPass)?<span className="form-text text-danger">Las contraseñas no coinciden</span>:<span className="form-text text-muted">Un correo electronico valido</span>
      let send;
       switch (this.state.Edit){
          case true:
          send=   <div className="kt-portlet">
            <div className=" col-md-12">
                <form className="kt-form kt-form--label-right">
                    <div className="kt-portlet__body">
                        <div className="form-group row">
                            <div className="col-md-6 form-row">
                                <label>Correo:</label>
                                <div className="input-group">
                                    <div className="input-group-prepend"><span className="input-group-text"><i className="la flaticon2-email"></i></span></div>
                                    <input type="text" className="form-control" placeholder={email}  ref={this.emailValue}/>
                                </div>
                                <span className="form-text text-muted">Un correo electronico valido</span>
                            </div>
                            <div className="col-md-6 form-row">
                                <label>Contraseña:</label>
                                <input class="form-control input input-danger " type="password"  ref={this.oldPasswordValue} />
                                {errorPass}
                            </div>
     
                        </div>
                        <div className="form-group row">
                           
                        <div className="col-md-6 form-row">
                                <label>Repita contraseña:</label>
                                <input class="form-control" type="password"  ref={this.passwordValue} />
                    
                                {errorPass}
     
                            </div>
                        </div>
     
                    
                    </div>
     
                    <div className="modal-footer">
                    <button type="reset" className="btn btn-danger"  ref={this.resetValue}>Resetear</button>
                    <button type="button" className="btn btn-primary"  ref={this.saveValue} onClick={()=>this.Edit(this.props.id)}>Editar</button>
                </div>
                </form>
            </div>
        </div>
            break;
      
          default:
              break;
      }
      

        return (
                   <div className="card">
                   <div className="card-body">
                  {send}
                    </div>
                   </div>
              
       );
    }
}


export default FormEditUser;