import React, { Component ,Fragment} from 'react';
import {Assing} from './../../../services/general';
import {getAllUsers} from './../../../services/user';
import routes from './../../../services/routes';
import {m_toast} from './../sweetalert';
import Spinner from './../../Spinner';
class AssingCard extends Component {
    state = {  
        users:[{id:"",user:{dni_rif:"cargando...",firstname:"",lastname:""}}],
        usersOk:false
    }
    searchRef = React.createRef();
    userRef = React.createRef();
componentDidMount(){
    let token = localStorage.getItem('token');
    getAllUsers(routes.routeAllUsers, token, respuesta => {
      var  res="";
        if (respuesta instanceof Error) {
        } else {
            console.log(respuesta)
         this.setState({users:[...respuesta.items]})

    }
    return res;
}).then(respuesta=>console.log(this.state.users))

}

    Assing = async () =>{

        let param = document.getElementById('users').value;
         if(this.userRef.current.value==""){
            m_toast('error',"aun no hay nada que asignar")
            this.setState({errorCode:true})
         }else{
            let token = 'Bearer' + " " + localStorage.getItem('token');
            this.setState({loading:true})
           await  Assing(routes.routeAsingCard,token,param,JSON.stringify({code:this.props.id}),respuesta => {
                if (respuesta instanceof Error) {
                    console.log(respuesta)
                    m_toast('error',respuesta)

                } else {
                    console.log(respuesta)
                    //if condicion
                    this.setState({errorCode:false});
                    m_toast('success',"Codigo cambiado")
                   // callback(param,this.emailValue.current.value)
                   
                }
            }).then(()=>{
                this.setState({loading:false})
            }
                
            )

           
         }
      
      }
    render() { 
 let classN = (this.state.usersOk)?"form-text text-muted":"form-text text-muted kt-spinner kt-spinner--sm kt-spinner--danger kt-spinner--left kt-spinner--select"


   return (    <Fragment>
            <div className="kt-portlet">
              <div className=" col-md-12">
                  <form className="kt-form kt-form--label-right">
                  <h4 className="pb-3 card-header ">Asignar a usuario</h4>
                  <div className="card">
           
                <div className="card-body ">
                <div className="form-group row">
                            <div className="col-md-6 form-row">
                                <label>Buscar:</label>
                                <input type="text" className="form-control" placeholder="Buscar" ref={this.searchRef} />
                                <span className="form-text text-muted">Escribe la cedula de usuario</span>
                            </div>
                            <div className="col-md-6 form-row">
                                <label>Usuarios:</label>
                              
               
                               <select className="form-control" ref={this.userRef} id="users">
                                {this.state.users.map((value,key)=>{
                                    return(
                                 
                                        <Fragment>
                                <option value={value.user.id}>
                                    {value.user.dni_rif} {value.user.firstname}
                                    </option>

                                        </Fragment>
                                    )
                                })}
                               </select>
                               
                               <small className="text-mutet">Usuarios</small>

                            </div>
                        </div>
                 </div>
                 </div>
                 <div className="kt-portlet__foot">
                        <div className="kt-form__actions">
                            <div className="row">
                                <div className="col-md-6">
                                </div>
                                <div className="col-md-3 offset-md-3 ">
                                   
                                    <button type="button" className="btn btn-primary  m--align-right" onClick ={()=>this.Assing()}>Asignar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                  </form>
              </div>
          </div>
          
             
       </Fragment>);
    }
}
 
export default AssingCard;