import React, { Component, Fragment } from 'react';
import {store} from './../../../services/resource'
class StudentCreate extends Component {
  
  state={

       
   }
       dniRef = React.createRef()
        firstNameRef = React.createRef()
        middleNameRef = React.createRef()
        lastNameRef = React.createRef()
        secondLastNameRef = React.createRef()

    Save = async () =>{
        console.log("here")

        let body = {
            "dni":this.dniRef.current.value,
            "nombre": this.firstNameRef.current.value,
            "segundo_nombre": this.middleNameRef.current.value,
            "apellido" :this.lastNameRef.current.value,
            "segundo_apellido":this.secondLastNameRef.current.value  
        }
       await store("alumno",localStorage.getItem('token'),body,(respuesta)=>{
        this.props.callback()
          console.log(respuesta)
        })
    }
    render() { 
        
        return (
           /*
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-xs-12">
                       
                            <div className="container-fluid">
                                <div className="row">
                                    <div className="col-xs-12 col-md-10 col-md-offset-1">
                                        <form>
                                            <fieldset>Datos del estudiante</fieldset>
                                            <div className="form-group label-floating">
                                                <label className="control-label">Identificacion</label>
                                                <input className="form-control" type="text" ref={this.dniRef}/>                       
                                            </div>
                                            <div className="form-group label-floating">
                                                <label className="control-label">Primer Nombre</label>
                                                <input className="form-control" type="text" ref={this.firstNameRef}/>
                                            </div> 
                                            <div className="form-group label-floating">
                                                <label className="control-label">Segundo Nombre</label>
                                                <input className="form-control" type="text" ref={this.middleNameRef}/>
                                            </div>
                                            <div className="form-group label-floating">
                                                <label className="control-label">Apellido</label>
                                                <input className="form-control" type="text"  ref={this.lastNameRef}/>
                                            </div>
                                            <div className="form-group label-floating">
                                                <label className="control-label">Segundo Apellido</label>
                                                <input className="form-control" type="text"  ref={this.secondLastNameRef}/>
                                            </div>
                                            <p class="text-center">
										    	<button type="button" class="btn btn-info btn-raised btn-sm" onClick={()=>this.Save()}><i class="zmdi zmdi-floppy" ></i> Guardar</button>
										    </p>                                             
                                        </form>
                                    </div>       
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            */
        <div className="container-fluid">
			<div className="row">
				<div className="col-xs-12">
					
					<div id="myTabContent" className="tab-content">
						<div className="tab-pane fade active in" id="new">
							<div className="container-fluid">
								<div className="row">
									<div className="col-xs-12 col-md-10 col-md-offset-1">
									    <form action="">
									    	<fieldset>Student Data</fieldset>
									    	<div className="form-group label-floating">
											  <label className="control-label">Name</label>
											  <input className="form-control" type="text"/>
											</div>
											<div className="form-group label-floating">
											  <label className="control-label">Last Name</label>
											  <input className="form-control" type="text"/>
											</div>
											<div className="form-group label-floating">
											  <label className="control-label">Address</label>
											  <textarea className="form-control"></textarea>
											</div>
											<div className="form-group label-floating">
											  <label className="control-label">Email</label>
											  <input className="form-control" type="text"/>
											</div>
											<div className="form-group label-floating">
											  <label className="control-label">Phone</label>
											  <input className="form-control" type="text"/>
											</div>
											<div className="form-group">
											  <label className="control-label">Birthday</label>
											  <input className="form-control" type="date"/>
											</div>
											<div className="form-group">
										        <label className="control-label">Gender</label>
										        <select className="form-control">
										          <option>Male</option>
										          <option>Female</option>
										        </select>
										    </div>
											<div className="form-group">
										      <label className="control-label">Photo</label>
										      <div>
										        <input type="text" readonly="" className="form-control" placeholder="Browse..."/>
										        <input type="file" />
										      </div>
										    </div>
										    <div className="form-group">
										        <label className="control-label">Student Type</label>
										        <select className="form-control">
										          <option>Old</option>
										          <option>New</option>
										        </select>
										    </div>
										    <div className="form-group">
										        <label className="control-label">Section</label>
										        <select className="form-control">
										          <option>1 grade</option>
										          <option>2 grade</option>
										          <option>3 grade</option>
										          <option>4 grade</option>
										          <option>5 grade</option>
										        </select>
										    </div>
										    <br/><br/>
										    <fieldset>Representative Data</fieldset>
										    <div className="form-group label-floating">
											  <label className="control-label">Representative DNI</label>
											  <input className="form-control" type="text"/>
											</div>
											<div className="form-group label-floating">
											  <label className="control-label">Representative Relationship</label>
											  <input className="form-control" type="text"/>
											</div>
										    <p className="text-center">
										    	<button href="#!" className="btn btn-info btn-raised btn-sm"><i className="zmdi zmdi-floppy"></i> Save</button>
										    </p>
                                            
									    </form>
									</div>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>              
		             
        );
    }
}
 
export default StudentCreate;