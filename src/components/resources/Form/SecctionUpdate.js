import React, { Component, Fragment } from 'react';
import {store,show} from './../../../services/resource';

import Spin from './../../Spinners';

class SecctionCreate extends Component {
  
  state={
  subject:{items:false},
  career:{items:false},
  profesor:{items:false},
  seccion:{items:false}

       
   }
       dniRef = React.createRef()
        firstNameRef = React.createRef()
        middleNameRef = React.createRef()
        lastNameRef = React.createRef()
        secondLastNameRef = React.createRef()

   componentDidMount(){

    let token = localStorage.getItem('token');
   
     show("seccion",localStorage.getItem("token"),this.props.id, respuesta => {
        if (respuesta instanceof Error) {
            console.log(respuesta)
          //  m_toast('error',respuesta)

        } else {
            console.log(respuesta)
            this.setState({seccion:respuesta.data})
           // m_toast('success',respuesta)

       
        }
    })
   

       this.props.callback2()

   this.setState({subject:this.props.subject})
    this.setState({career:this.props.career})
    this.setState({profesor:this.props.profesor})
   }


    Save = async () =>{
        console.log("here")

        let body = {
            "dni":this.dniRef.current.value,
            "nombre": this.firstNameRef.current.value,
            "segundo_nombre": this.middleNameRef.current.value,
            "apellido" :this.lastNameRef.current.value,
            "segundo_apellido":this.secondLastNameRef.current.value  
        }
       await store("seccion",localStorage.getItem('token'),body,(respuesta)=>{
        this.props.callback()
          console.log(respuesta)
        })
    }
    render() { 
        console.log(this.props.subject,this.props.career,this.props.profesor)
        console.log(4,this.state.subject)
        let subject = <Spin></Spin>;
        if(this.state.subject.ok!=false){
            console.log(5,this.state.subject)
        }
      
      
        let career = (this.state.career.items!=false)? 
        <select>
      {this.state.career.items.data.map((value,key)=>{
                return(
                    <option>hola</option>
                )
                
            })
      }
              </select>
        :<Spin></Spin>

        let profesor = (this.state.profesor.items!=false)? 
        <select>
      { this.state.profesor.items.data.map((value,key)=>{
                return(
                    <option>hola</option>
                )
                
            })
      }
              </select>
        :<Spin></Spin>
        
        return (
           
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-xs-12">
                       
                            <div className="container-fluid">
                                <div className="row">
                                    <div className="col-xs-12 col-md-10 col-md-offset-1">
                                        <form>
                                            <fieldset>Datos del estudiante</fieldset>
                                            <div className="form-group label-floating">
                                                <label className="control-label">Alias</label>
                                                <input className="form-control" type="text" ref={this.dniRef}/>                       
                                            </div>
                                            <div className="form-group label-floating">
                                                <label className="control-label">Lapso</label>
                                                <input className="form-control" type="text" ref={this.firstNameRef}/>
                                            </div> 
                                            
                                            <div className="form-group label-floating">
                                                <label className="control-label">Asignaturas</label>
                                         
                                             {subject}
                                          
                                            </div>
                                            <div className="form-group label-floating">
                                                <label className="control-label">Profesor</label>
                                         
                                             {profesor}
                                          
                                            </div>
                                            <div className="form-group label-floating">
                                                <label className="control-label">Carrera</label>
                                         
                                             {career}
                                          
                                            </div>
                                            <p class="text-center">
										    	<button type="button" class="btn btn-info btn-raised btn-sm" onClick={()=>this.Save()}><i class="zmdi zmdi-floppy" ></i> Guardar</button>
										    </p>                                             
                                        </form>
                                    </div>       
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                          
        );
    }
}
 
export default SecctionCreate;