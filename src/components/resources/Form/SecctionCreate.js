import React, { Component, Fragment } from 'react';
import {store} from './../../../services/resource';
import Spin from './../../Spinners';

class SecctionCreate extends Component {
  
  state={
  subject:{items:false},
  career:{items:false},
  profesor:{items:false},

  focusAlias: false,
  focusLapso: false
  

       
   }
       dniRef = React.createRef()
        firstNameRef = React.createRef()
        middleNameRef = React.createRef()
        lastNameRef = React.createRef()
        secondLastNameRef = React.createRef()

   componentDidMount(){
       this.props.callback2()

   this.setState({subject:this.props.subject})
    this.setState({career:this.props.career})
    this.setState({profesor:this.props.profesor})
   }
    Save = async () =>{
        console.log("here")

        let body = {
            "dni":this.dniRef.current.value,
            "nombre": this.firstNameRef.current.value,
            "segundo_nombre": this.middleNameRef.current.value,
            "apellido" :this.lastNameRef.current.value,
            "segundo_apellido":this.secondLastNameRef.current.value  
        }
       await store("seccion",localStorage.getItem('token'),body,(respuesta)=>{
        this.props.callback()
          console.log(respuesta)
        })
    }

   
    render() { 
        console.log(this.props.subject,this.props.career,this.props.profesor)
        console.log(4,this.state.subject)
        let subject = <Spin></Spin>;
        if(this.state.subject.ok!=false){
            console.log(5,this.state.subject)
        }
      
      
        let career = (this.state.career.items!=false)? 
        <select>
      {this.state.career.items.data.map((value,key)=>{
                return(
                    <option defaultValue={value.id}>{value.nombre}</option>
                )
                
            })
      }
              </select>
        :<Spin></Spin>

        let profesor = (this.state.profesor.items!=false)? 
        <select>
      { this.state.profesor.items.data.map((value,key)=>{
                return(
                    <option>hola</option>
                )
                
            })
      }
              </select>
        :<Spin></Spin>
    
        let focusAliasI = (this.state.focusAlias) ? 'form-group label-floating is-focused' : 'form-group label-floating'
        let labelAlias = (this.state.focusAlias) ?<label className="control-label">Alias</label>:""
        let placeholderAlias = (this.state.focusAlias) ?"":"Alias"

        let focusLapsoI = (this.state.focusLapso) ? 'form-group label-floating is-focused' : 'form-group label-floating'
        let labelLapso = (this.state.focusLapso) ?<label className="control-label">Lapso</label>:""
        let placeholderLapso = (this.state.focusLapso) ?"":"Lapso"
        
        return (
           
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-xs-12">
                       
                            <div className="container-fluid">
                                <div className="row">
                                    <div className="col-xs-12 col-md-10 col-md-offset-1">
                                        <form>
                                            <fieldset>Datos de la carrera</fieldset>
                                            <div className={focusAliasI}>
                                                {labelAlias}
                                                <input className="form-control" placeholder={placeholderAlias} type="text" ref={this.dniRef} onFocus={() => this.setState({focusAlias:true})} onBlur={() =>  this.setState({focusAlias:false})}/>                       
                                            </div>
                                            <div className={focusLapsoI}>
                                               {labelLapso} 
                                                <input className="form-control" placeholder={placeholderLapso} type="text" ref={this.firstNameRef} onFocus={() => this.setState({focusLapso:true})} onBlur={() =>  this.setState({focusLapso:false})}/>
                                            </div> 
                                            
                                            <div className="form-group label-floating">
                                                <label className="control-label">Asignaturas</label>
                                         
                                             {subject}
                                          
                                            </div>
                                            <div className="form-group label-floating">
                                                <label className="control-label">Profesor</label>
                                         
                                             {profesor}
                                          
                                            </div>
                                            <div className="form-group label-floating">
                                                <label className="control-label">Carrera</label>
                                         
                                             {career}
                                          
                                            </div>
                                            <p class="text-center">
										    	<button type="button" class="btn btn-info btn-raised btn-sm" onClick={()=>this.Save()}><i class="zmdi zmdi-floppy" ></i> Guardar</button>
										    </p>                                             
                                        </form>
                                    </div>       
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                          
        );
    }
}
 
export default SecctionCreate;