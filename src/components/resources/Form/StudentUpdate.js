import React, { Component, Fragment } from 'react';
import routes from './../../../services/routes';
import {m_toast} from './../sweetalert';
import {findOrChange,findOrChangeWith} from './../../../services/general';
import {update,show} from './../../../services/resource';
import Swal from 'sweetalert2';
import Spin from './../../Spinners';

class SubjectUpdate extends Component {

    state = {
        student:false
       }
       dniRef = React.createRef()
       firstNameRef = React.createRef()
       middleNameRef = React.createRef()
       lastNameRef = React.createRef()
       secondLastNameRef = React.createRef()

 
 
        Update = async () =>{
         
 
            let body = {
                "dni":this.dniRef.current.value,
                "nombre": this.firstNameRef.current.value,
                "segundo_nombre": this.middleNameRef.current.value,
                "apellido" :this.lastNameRef.current.value,
                "segundo_apellido":this.secondLastNameRef.current.value  
            }
         console.log(body)
        await update("alumno",localStorage.getItem('token'),this.props.id,body,(respuesta)=>{
           console.log(respuesta)
         })
     }
    componentDidMount(){
        let token = localStorage.getItem('token');
       
         show("alumno",localStorage.getItem("token"),this.props.id, respuesta => {
            if (respuesta instanceof Error) {
                console.log(respuesta)
              //  m_toast('error',respuesta)

            } else {
                console.log(respuesta)
                this.setState({student:respuesta.data})
               // m_toast('success',respuesta)

           
            }
        })
       
        
    }
   

    render() {
        let name =(this.state.student)?
        <Fragment>
        <label className="control-label">Nombre</label>
         <input className="form-control" type="text" ref={this.name} value={this.state.student.nombre}/>
        </Fragment>
         :<Spin></Spin>                    
        let registration =(this.state.student)? 
        <Fragment>
        <label className="control-label">Matricula</label>
        <input className="form-control" type="text" ref={this.registration} value={this.state.student.matricula}/>
        </Fragment>

        :<Spin></Spin>    

        let avaliable =(this.state.student)?  
        <Fragment>
        <label className="control-label">Disponibilidad</label>
        <input className="form-control" type="text" ref={this.avaliable} value={this.state.student.disponibilidad}/>
        </Fragment>
        :<Spin></Spin>                    
        let start_date =(this.state.student)? <input className="form-control" type="date"  ref={this.start_date} value={this.state.student.fecha_inicio}/>:<Spin></Spin>                    
        let end_date =(this.state.student)? <input className="form-control" type="date"  ref={this.end_date} value={this.state.student.fecha_fin}/>:<Spin></Spin>                    

 
       return(
           
        <div className="container-fluid">
                    <div className="row">
                        <div className="col-xs-12">
                       
                            <div className="container-fluid">
                                <div className="row">
                                    <div className="col-xs-12 col-md-10 col-md-offset-1">
                                        <form>
                                            <fieldset>Datos del estudiante</fieldset>
                                            <div className="form-group label-floating">
                                                <label className="control-label">Identificacion</label>
                                                <input className="form-control" type="text" ref={this.dniRef}/>                       
                                            </div>
                                            <div className="form-group label-floating">
                                                <label className="control-label">Primer Nombre</label>
                                                <input className="form-control" type="text" ref={this.firstNameRef}/>
                                            </div> 
                                            <div className="form-group label-floating">
                                                <label className="control-label">Segundo Nombre</label>
                                                <input className="form-control" type="text" ref={this.middleNameRef}/>
                                            </div>
                                            <div className="form-group label-floating">
                                                <label className="control-label">Apellido</label>
                                                <input className="form-control" type="text"  ref={this.lastNameRef}/>
                                            </div>
                                            <div className="form-group label-floating">
                                                <label className="control-label">Segundo Apellido</label>
                                                <input className="form-control" type="text"  ref={this.secondLastNameRef}/>
                                            </div>
                                            <p class="text-center">
										    	<button type="button" class="btn btn-info btn-raised btn-sm" onClick={()=>this.Update()}><i class="zmdi zmdi-floppy" ></i> Guardar</button>
										    </p>                                             
                                        </form>
                                    </div>       
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
       );
              
    
    }
}


export default SubjectUpdate;