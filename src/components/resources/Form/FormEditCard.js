import React, { Component, Fragment } from 'react';
import routes from './../../../services/routes';
import {m_toast} from './../sweetalert';
import {findOrChange,findOrChangeWith} from './../../../services/general';
import Swal from 'sweetalert2';
import Qr from './../../Qr';
import Spinner from './../../MiniSpin';

class FormEditCard extends Component {

    state = {
        preview:false,
        Edit:true,
        userEdit:"",
        errorCode:false,
        errorUser:false,
        loading:false,
        data:[]}
        codeRef = React.createRef();
        saveRef = React.createRef();
        resetRef = React.createRef();
    componentDidMount(){
        let token = localStorage.getItem('token');
       
         findOrChange("GET",routes.routeTheCard,token,this.props.id, respuesta => {
            if (respuesta instanceof Error) {
                console.log(respuesta)
                m_toast('error',respuesta)

            } else {
                console.log(respuesta)
                //if condicion
                this.setState({errorCode:false});
                this.setState({data:{...respuesta}});
                this.setState({dataOk:true});
               // callback(param,this.emailValue.current.value)
               
            }
        })
       
        
    }
    Edit = async (param) =>{
        let validation =true;
         if(this.codeRef.current.value.length<8){
            m_toast('error',"minimo 8 caracteres")
            this.setState({errorCode:true})
         }else{
            let token = localStorage.getItem('token');
            this.setState({loading:true})
           await  findOrChangeWith("PUT",routes.routeTheCard,token,this.props.id,JSON.stringify({"code":this.codeRef.current.value,"status":"ACTIVE"}), respuesta => {
                if (respuesta instanceof Error) {
                    console.log(respuesta)
                    m_toast('error',respuesta)

                } else {
                    this.props.Edited(respuesta)
                    console.log(respuesta)
                    //if condicion
                    this.setState({errorCode:false});
                    m_toast('success',"Codigo cambiado")
                   // callback(param,this.emailValue.current.value)
                   
                }
            }).then(()=>{
                this.setState({loading:false})
            }
                
            )

           
         }
      
      }

      Preview = (e) =>{
       this.setState({preview:e.target.value})
      }

    render() {
      let preview = (this.state.preview!=false)?
     
         
                    <Fragment>
                        <Qr code ={this.state.preview}></Qr>
                    </Fragment>
                :<Fragment>
         
         </Fragment>
          
        let button = (this.state.loading)?  <button type="button" className="btn btn-primary kt-spinner kt-spinner--light kt-spinner--right"  ref={this.saveRef} onClick={()=>this.Edit(this.props.id)}>Editar</button>:
          <button type="button" className="btn btn-primary"  ref={this.saveRef} onClick={()=>this.Edit(this.props.id)}>Editar</button>
       let val = (this.state.dataOk)?<input type="text" className="form-control" placeholder="insert your code.." defaultValue={this.state.data.code} ref={this.codeRef} onKeyUp={this.Preview}/>:<Spinner></Spinner>

       
        return (
                   <div className="card">
                   <div className="card-body">
                   <div className="kt-portlet">
            <div className=" col-md-12">
                <form className="kt-form kt-form--label-right">
                <div className="form-group row">
                            <div className="col-md-6 form-row">

                                <label>Codigo:</label>
                                <div className="input-group">
                                    <div className="input-group-prepend"><span className="input-group-text"><i className="fas fa-barcode"></i></span></div>
{val}                              </div>
                                <span className="form-text text-muted">minimo 8 caracteres</span>
                            </div>
                            <div className="col-md-6 form-row">
                           {preview}
                            </div>
     
                        </div>
     
                    <div className="modal-footer">
                    <button type="reset" className="btn btn-danger"  ref={this.resetRef}>Resetear</button>
                      {button}
                </div>
                </form>
            </div>
        </div>
                    </div>
                   </div>
              
       );
    }
}


export default FormEditCard;