import React, { Component, Fragment } from 'react';
import {store} from './../../../services/resource'
class StudentCreate extends Component {
  
  state={
    
    focusInput: false
       
   }
      nameRef = React.createRef()
      

    Save = async () =>{
        console.log("here")

        let body = {
            "nombre":this.nameRef.current.value,
           
        }
       await store("carrera",localStorage.getItem('token'),body,(respuesta)=>{
        this.props.callback()
          console.log(respuesta)
        })
    }

    FocusActive = () =>{
       this.setState({
           "focusInput":true
        })
    }

    FocusDesactive = () => {
        this.setState({
            "focusInput":false
         })
    }

    render() { 
        let label = (this.state.focusInput) ?<label className="control-label">Nombre</label>:""
        let focus = (this.state.focusInput) ? 'form-group label-floating is-focused' : 'form-group label-floating'
        let placeholder = (this.state.focusInput) ?"":"Nombre"
        
        return (
           
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-xs-12">
                       
                            <div className="container-fluid">
                                <div className="row">
                                    <div className="col-xs-12 col-md-10 col-md-offset-1">
                                        <form>
                                            <fieldset>Datos de la carrera</fieldset>
                                            <div className={focus}>
                                                {label}
                                                <input className="form-control" type="text" placeholder={placeholder} ref={this.nameRef} onFocus={() => this.FocusActive()} onBlur={() => this.FocusDesactive()} />                       
                                            </div>
                                            
                                            <p class="text-center">
										    	<button type="button" class="btn btn-info btn-raised btn-sm" onClick={()=>this.Save()}><i class="zmdi zmdi-floppy" ></i> Guardar</button>
										    </p>                                             
                                        </form>
                                    </div>       
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                          
        );
    }
}
 
export default StudentCreate;