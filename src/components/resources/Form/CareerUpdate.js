import React, { Component, Fragment } from 'react';
import routes from './../../../services/routes';
import {m_toast} from './../sweetalert';
import {findOrChange,findOrChangeWith} from './../../../services/general';
import {update,show} from './../../../services/resource';
import Swal from 'sweetalert2';
import Spin from './../../Spinners';

class SubjectUpdate extends Component {

    state = {
        career:false
       }
      nameRef = React.createRef()
     

 
 
        Update = async () =>{
         
 
            let body = {
                "nombre":this.nameRef.current.value,
               
            }
         console.log(body)
        await update("carrera",localStorage.getItem('token'),this.props.id,body,(respuesta)=>{
           console.log(respuesta)
         })
     }
    componentDidMount(){
        let token = localStorage.getItem('token');
       
         show("carrera",localStorage.getItem("token"),this.props.id, respuesta => {
            if (respuesta instanceof Error) {
                console.log(respuesta)
              //  m_toast('error',respuesta)

            } else {
                console.log(respuesta)
                this.setState({career:respuesta.data})
               // m_toast('success',respuesta)

           
            }
        })
       
        
    }
   

    render() {
        let name =(this.state.career)?
        <Fragment>
        <label className="control-label">Nombre</label>
         <input className="form-control" type="text" ref={this.nameRef} defaultValue={this.state.career.nombre}/>
        </Fragment>
         :<Spin></Spin>                    
        
 
       return(
           
        <div className="container-fluid">
        <div className="row">
            <div className="col-xs-12">
           
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-xs-12 col-md-10 col-md-offset-1">
                            <form>
                                <fieldset>Datos de la carrera</fieldset>
                                <div className="form-group label-floating">
                                 {name}     
                                 </div>
                                
                                <p class="text-center">
                                    <button type="button" class="btn btn-info btn-raised btn-sm" onClick={()=>this.Update()}><i class="zmdi zmdi-floppy" ></i> Guardar</button>
                                </p>                                             
                            </form>
                        </div>       
                    </div>
                </div>
            </div>
        </div>
    </div>
       );
              
    
    }
}


export default SubjectUpdate;