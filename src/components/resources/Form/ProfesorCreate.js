import React, { Component, Fragment } from 'react';
import {store} from './../../../services/resource'
class StudentCreate extends Component {
  
  state={

       
   }
       dniRef = React.createRef()
        firstNameRef = React.createRef()
        middleNameRef = React.createRef()
        lastNameRef = React.createRef()
        secondLastNameRef = React.createRef()

    Save = async () =>{
        console.log("here")

        let body = {
            "dni":this.dniRef.current.value,
            "nombre": this.firstNameRef.current.value,
            "segundo_nombre": this.middleNameRef.current.value,
            "apellido" :this.lastNameRef.current.value,
            "segundo_apellido":this.secondLastNameRef.current.value  
        }
       await store("profesor",localStorage.getItem('token'),body,(respuesta)=>{
        this.props.callback()
          console.log(respuesta)
        })
    }
    render() { 
        
        return (
           
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-xs-12">
                       
                            <div className="container-fluid">
                                <div className="row">
                                    <div className="col-xs-12 col-md-10 col-md-offset-1">
                                        <form>
                                            <fieldset>Datos del profesor</fieldset>
                                            <div className="form-group label-floating">
                                                <label className="control-label">Identificacion</label>
                                                <input className="form-control" type="text" ref={this.dniRef}/>                       
                                            </div>
                                            <div className="form-group label-floating">
                                                <label className="control-label">Primer Nombre</label>
                                                <input className="form-control" type="text" ref={this.firstNameRef}/>
                                            </div> 
                                            <div className="form-group label-floating">
                                                <label className="control-label">Segundo Nombre</label>
                                                <input className="form-control" type="text" ref={this.middleNameRef}/>
                                            </div>
                                            <div className="form-group label-floating">
                                                <label className="control-label">Apellido</label>
                                                <input className="form-control" type="text"  ref={this.lastNameRef}/>
                                            </div>
                                            <div className="form-group label-floating">
                                                <label className="control-label">Segundo Apellido</label>
                                                <input className="form-control" type="text"  ref={this.secondLastNameRef}/>
                                            </div>
                                            <p class="text-center">
										    	<button type="button" class="btn btn-info btn-raised btn-sm" onClick={()=>this.Save()}><i class="zmdi zmdi-floppy" ></i> Guardar</button>
										    </p>                                             
                                        </form>
                                    </div>       
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                          
        );
    }
}
 
export default StudentCreate;