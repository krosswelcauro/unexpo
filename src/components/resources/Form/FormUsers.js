import React, { Component,Fragment } from 'react';
import routes from './../../../services/routes';
import {findUser} from './../../../services/user';
import Spinner from './../../Spinner';
import Swal from 'sweetalert2';

class FormUsers extends Component {
    state = {
        data:false,
        userEdit:"",
        errorPass:false,
        errorUser:false,
        loading:false,
        findOne:[],
     
    }
  
        FindUser = async(find) =>{
            find = find.id
            
             let token =  localStorage.getItem('token');
             let users =  await findUser(routes.routeFindUser, token, find,respuesta => {
                 if (respuesta instanceof Error) {
                     console.log(respuesta)
                 } else {
                  
                 this.setState({findOne:respuesta})
                 this.setState({loading:true})
             
                 }
             }).then()
         
         }
    componentDidMount () {
        const  handle  = this.props.id
        //const { fromNotifications } = this.props.location.state  
       console.log(handle)
            
        let token = 'Bearer' + " " + localStorage.getItem('token');
        let users =   findUser(routes.routeFindUser, token, Number(handle),respuesta => {
            if (respuesta instanceof Error) {
                console.log(respuesta)
            } else {
              
            this.setState({findOne:respuesta})
            this.setState({loading:true})
        
            }
        }).then()
    
    }
      
    
   
    render() {

    let firstname;
    let lastname;
    let email;
    let birthday;
    let address;
    let phone;
    let codeCard;
    let statusCard;
     if(this.state.loading){
  

        firstname = this.state.findOne.user.firstname 
        lastname  = this.state.findOne.user.lastname
        email = this.state.findOne.email
        birthday = this.state.findOne.user.birthday
        address = this.state.findOne.user.address
        phone =this.state.findOne.user.phone
       if(this.state.findOne.user.card == null){
        codeCard = "Sin asignar"
        statusCard = "No obtenida"
       }else{
        codeCard = this.state.findOne.user.card.code
        statusCard = this.state.findOne.user.card.status.name
      
       }
     }else{
        return (<Spinner></Spinner>)
     }
  
        return (
         <Fragment>
              <div className="kt-portlet">
                <div className=" col-md-12">
                    <form className="kt-form kt-form--label-right">
                    <div className="card">
             <div className="card-body ">
             <h4 className="pb-3 ">Datos Personales</h4>
                <div className="container">
                <div className="form-row ">
               <h5 className="col-md-5 text-left mr-2 mb-2">Nombre completo :{firstname } {lastname}</h5>
               <h5 className="col-md-5 text-left mb-2">Correo :{email}</h5>
      
            
              <h5 className="col-md-5 text-left mr-2 mb-2">Fecha de nacimiento:{birthday}</h5>
               <h5 className="col-md-5 text-left mb-2">Direccion:{address}</h5>
          
           
              <h5 className="col-md-5 text-left mr-2 mb-2">Telefono:{phone}</h5>
               <h5 className="col-md-5 text-left mb-2"></h5>
          
              </div>
                </div>
             
             
             </div>

             <div className="card-body justify-content-center">
                 <h4 className="pb-3 ">Datos de Tarjeta</h4>
                 <div className="container">

              <div className="form-row ">
               <h5 className="col-md-5 text-left mr-2 mb-2">Codigo de Tarjeta:{codeCard}</h5>
               <h5 className="col-md-5 text-left mb-2">Estado de Tarjeta:{statusCard}</h5>
           
              </div>
             
              </div>
             </div>
            </div>
                    </form>
                </div>
            </div>
            
               
         </Fragment>
            );
    }
}

export default FormUsers;