import React, { Component, Fragment } from 'react';
import routes from './../../../services/routes';
import {m_toast} from './../sweetalert';
import {findOrChange,findOrChangeWith} from './../../../services/general';
import {update,show} from './../../../services/resource';
// import Swal from 'sweetalert2-react';
import Spin from './../../Spinners';

class SubjectUpdate extends Component {

    state = {
        subject:false
       }
        name = React.createRef()
        registration = React.createRef()
        avaliable = React.createRef()
        start_date = React.createRef()
        end_date = React.createRef()
 
        Update = async () =>{
         
 
         let body = {
             "nombre": this.name.current.value,
             "matricula": this.registration.current.value,
             "disponibilidad" :parseInt(this.avaliable.current.value),
             "fecha_inicio":this.start_date.current.value ,
             "fecha_fin":this.end_date.current.value  
         }
         console.log(body)
        await update("asignatura",localStorage.getItem('token'),this.props.id,body,(respuesta)=>{
           console.log(respuesta)
         })
     }
    componentDidMount(){
        let token = localStorage.getItem('token');
       
         show("asignatura",localStorage.getItem("token"),this.props.id, respuesta => {
            if (respuesta instanceof Error) {
                console.log(respuesta)
              //  m_toast('error',respuesta)

            } else {
                console.log(respuesta)
                this.setState({subject:respuesta.data})
               // m_toast('success',respuesta)

           
            }
        })
       
        
    }
   

    render() {
        let name =(this.state.subject)?
        <Fragment>
        <label className="control-label">Nombre</label>
         <input className="form-control" type="text" ref={this.name} value={this.state.subject.nombre}/>
        </Fragment>
         :<Spin></Spin>                    
        let registration =(this.state.subject)? 
        <Fragment>
        <label className="control-label">Matricula</label>
        <input className="form-control" type="text" ref={this.registration} value={this.state.subject.matricula}/>
        </Fragment>

        :<Spin></Spin>    

        let avaliable =(this.state.subject)?  
        <Fragment>
        <label className="control-label">Disponibilidad</label>
        <input className="form-control" type="text" ref={this.avaliable} value={this.state.subject.disponibilidad}/>
        </Fragment>
        :<Spin></Spin>                    
        let start_date =(this.state.subject)? <input className="form-control" type="date"  ref={this.start_date} value={this.state.subject.fecha_inicio}/>:<Spin></Spin>                    
        let end_date =(this.state.subject)? <input className="form-control" type="date"  ref={this.end_date} value={this.state.subject.fecha_fin}/>:<Spin></Spin>                    

 
       return(
           
        <div className="container-fluid">
        <div className="row">
            <div className="col-xs-12">
           
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-xs-12 col-md-10 col-md-offset-1">
                            <form>
                                <fieldset>Datos de la asignatura</fieldset>
                                <div className="form-group label-floating">
                                    {name}
                                </div>
                                <div className="form-group label-floating">
                                   
                                    {registration}
                                </div> 
                                <div className="form-group label-floating">
                                   {avaliable}
                                </div>
                                <div className="form-group label-floating">
                                    <label className="control-label">Fecha Inicio</label>
                                    {start_date}
                                </div>
                                <div className="form-group label-floating">
                                    <label className="control-label">Fecha Fin</label>
                                    {end_date}
                                </div>
                                <p class="text-center">
                                    <button type="button" className="btn btn-info btn-raised btn-sm" onClick={()=>this.Update()}><i class="zmdi zmdi-floppy" ></i> Guardar</button>
                                </p>                                             
                            </form>
                        </div>       
                    </div>
                </div>
            </div>
        </div>
    </div>
       );
              
    
    }
}


export default SubjectUpdate;