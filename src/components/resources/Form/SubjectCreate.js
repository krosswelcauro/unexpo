import React, { Component, Fragment } from 'react';
import {store} from './../../../services/resource'
class SubjectCreate extends Component {
  
  state={
      focusNombre: false,
      focusMatricula: false,
      focusDisponibilidad: false
      
   }
       name = React.createRef()
       registration = React.createRef()
       avaliable = React.createRef()
       start_date = React.createRef()
       end_date = React.createRef()

    Save = async () =>{
        

        let body = {
            "nombre": this.name.current.value,
            "matricula": this.registration.current.value,
            "disponibilidad" :parseInt(this.avaliable.current.value),
            "fecha_inicio":this.start_date.current.value ,
            "fecha_fin":this.end_date.current.value  
        }
        console.log(body)
       
       await store("asignatura",localStorage.getItem('token'),body,(respuesta)=>{
        this.props.callback()
          console.log(respuesta)
        })
    }
    render() { 

        let focusNombre = (this.state.focusNombre) ? 'form-group label-floating is-focused' : 'form-group label-floating'
        let labelNombre = (this.state.focusNombre) ?<label className="control-label">Nombre</label>:""
        let placeholderNombre = (this.state.focusNombre) ?"":"Nombre"

        let focusMatricula = (this.state.focusMatricula) ? 'form-group label-floating is-focused' : 'form-group label-floating'
        let labelMatricula = (this.state.focusMatricula) ?<label className="control-label">Matricula</label>:""
        let placeholderMatricula = (this.state.focusMatricula) ?"":"Matricula"

        let focusDisponibilidad = (this.state.focusDisponibilidad) ? 'form-group label-floating is-focused' : 'form-group label-floating'
        let labelDisponibilidad = (this.state.focusDisponibilidad) ?<label className="control-label">Disponibilidad</label>:""
        let placeholderDisponibilidad = (this.state.focusDisponibilidad) ?"":"Disponibilidad"
        
        return (
           
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-xs-12">
                       
                            <div className="container-fluid">
                                <div className="row">
                                    <div className="col-xs-12 col-md-10 col-md-offset-1">
                                        <form>
                                            <fieldset>Datos de la asignatura</fieldset>
                                            <div className={focusNombre}>
                                                {labelNombre}
                                                <input className="form-control" placeholder={placeholderNombre} type="text" ref={this.name} onFocus={() => this.setState({focusNombre:true})} onBlur={() =>  this.setState({focusNombre:false})}/>                       
                                            </div>
                                            <div className={focusMatricula}>
                                                {labelMatricula}
                                                <input className="form-control" placeholder={placeholderMatricula} type="text" ref={this.registration} onFocus={() => this.setState({focusMatricula:true})} onBlur={() =>  this.setState({focusMatricula:false})}/>
                                            </div> 
                                            <div className={focusDisponibilidad}>
                                                {labelDisponibilidad}
                                                <input className="form-control" placeholder={placeholderDisponibilidad} type="text" ref={this.avaliable} onFocus={() => this.setState({focusDisponibilidad:true})} onBlur={() =>  this.setState({focusDisponibilidad:false})}/>
                                            </div>
                                            <div className="form-group label-floating">
                                                <label className="control-label">Fecha Inicio</label>
                                                <input className="form-control" type="date"  ref={this.start_date}/>
                                            </div>
                                            <div className="form-group label-floating">
                                                <label className="control-label">Fecha Fin</label>
                                                <input className="form-control" type="date"  ref={this.end_date}/>
                                            </div>
                                            <p class="text-center">
										    	<button type="button" className="btn btn-info btn-raised btn-sm" onClick={()=>this.Save()}><i class="zmdi zmdi-floppy" ></i> Guardar</button>
										    </p>                                             
                                        </form>
                                    </div>       
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                          
        );
    }
}
 
export default SubjectCreate;