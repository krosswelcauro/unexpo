import React, { Component, Fragment } from 'react';
import routes from './../../../services/routes';
import {m_toast} from './../sweetalert';
import {findOrChange,findOrChangeWith,getAll} from './../../../services/general';
import Swal from 'sweetalert2';
import Qr from './../../Qr';
import Spinner from './../../MiniSpin';
import {paises} from './../../../services/paises'


class FormEditCard extends Component {

    state = {
        preview:false,
        Edit:true,
        userEdit:"",
        errorCode:false,
        errorUser:false,
        loading:false,
        data:false,
        paises:false,
        paisesOk:false,
        currency:false}
        nro_ctaRef = React.createRef();
        nameRef = React.createRef();
        dniRef = React.createRef();
        currencyRef = React.createRef();
        bankRef = React.createRef();
        typeRef = React.createRef();

    componentDidMount(){
        let token = localStorage.getItem('token');
         
        getAll(routes.currency,token,respuesta=>{
            if (respuesta instanceof Error) {
           
                m_toast('error',respuesta)

            } else {
                this.setState({currency:{...respuesta}})
                
                //if condicion
              
               // callback(param,this.emailValue.current.value)
               
            }
        })
         findOrChange("GET",routes.routeTheBank2,token,this.props.id, respuesta => {
            if (respuesta instanceof Error) {
          
                m_toast('error',respuesta)

            } else {
              
                //if condicion
                this.setState({errorCode:false});
                this.setState({data:{...respuesta}});
                this.setState({dataOk:true});
               // callback(param,this.emailValue.current.value)
               
            }
        }).then(()=>{})
     
      
        
    }
    Edit = async (param) =>{
            let token = localStorage.getItem('token');
            this.setState({loading:true})
           if(this.nro_ctaRef.current.value ==""||
            this.nameRef.current.value  ==""||
            this.dniRef.current.value  ==""||
            this.bankRef.current.value  ==""||
            this.currencyRef.current.value  =="")
            {
                m_toast('error',"Algunos campos estan vacios")

            }else{
                await  findOrChangeWith("PUT",routes.routeTheBank2,token,this.props.id,
                JSON.stringify({
                 "account_number": this.nro_ctaRef.current.value,
                 "bank_name": this.bankRef.current.value,
                 "dni_rif": this.dniRef.current.value,
                 "owner_name": this.nameRef.current.value,
                 "currency": parseInt(this.currencyRef.current.value)
             })
                , respuesta => {


                     if (respuesta instanceof Error) {
                        
                         m_toast('error',respuesta)
                     } else {
                        
                          console.log(respuesta)
                         m_toast('success',"Cuenta registrada")
                        // callback(param,this.emailValue.current.value)
                     }
                 }).then(()=>{
                     this.setState({loading:false})

                     console.log(this.state.data)
                 }
                     
                 )
            }
         
      }

      Preview = (e) =>{
       this.setState({preview:e.target.value})
      }

    render() {
     let nro_cta = (this.state.dataOk)?<input type="text" className="form-control" defaultValue={this.state.data.account_number} ref={this.nro_ctaRef}/>:<Spinner></Spinner>
     let bank = (this.state.dataOk)?<input type="text" className="form-control" defaultValue={this.state.data.bank_name} ref={this.bankRef}/>:<Spinner></Spinner>
     let name = (this.state.dataOk)?<input type="text" className="form-control" defaultValue={this.state.data.owner_name} ref={this.nameRef}/>:<Spinner></Spinner>
     let currency = (this.state.dataOk)?<input type="text" className="form-control" defaultValue={this.state.data.currency.value} ref={this.currencyRef}/>:<Spinner></Spinner>
     let dni = (this.state.dataOk)?<input type="text" className="form-control" defaultValue={this.state.data.dni_rif} ref={this.dniRef}/>:<Spinner></Spinner>
     let type = (this.state.currency!=false)?
     <select className="form-control input-sm" ref={this.currencyRef}>
         {this.state.currency.items.map((value,key)=>{
             return(
                <Fragment>
                    <option value={value.id}> 
                        {value.iso}
                    </option>
                </Fragment>
                    
             )
         })}
     </select>

     :<Spinner></Spinner>

        let button = (this.state.loading)?  <button type="button" className="btn btn-primary kt-spinner kt-spinner--light kt-spinner--right"  ref={this.saveRef} onClick={()=>this.Edit(this.props.id)}>Editar</button>:
          <button type="button" className="btn btn-primary"  ref={this.saveRef} onClick={()=>this.Edit(this.props.id)}>Editar</button>
       let val = (this.state.dataOk)?<input type="text" className="form-control" placeholder="insert your code.." defaultValue={this.state.data.code} ref={this.codeRef} onKeyUp={this.Preview}/>:<Spinner></Spinner>

       
        return (
                   <div className="card">
                   <div className="card-body">
                   <div className="kt-portlet">
            <div className=" col-md-12">
                <form className="kt-form kt-form--label-right">
                <div className="form-group row">
                            <div className="col-md-6 form-row">

                                <label>Numero de Cuenta:</label>
                                <div className="input-group">
                                    <div className="input-group-prepend"></div>
{nro_cta}                              </div>
                                <span className="form-text text-muted">minimo 8 caracteres</span>
                            </div>
                            <div className="col-md-6 form-row">

<label>Nombre del banco:</label>
<div className="input-group">
    <div className="input-group-prepend"></div>
{bank}                              </div>
<span className="form-text text-muted">nombre del banco</span>
</div>
     
                        </div>

                        <div className="form-group row">
                            <div className="col-md-6 form-row">

                                <label>Identicicaicon:</label>
                                <div className="input-group">
                                    <div className="input-group-prepend"></div>
{dni}                              </div>
                                <span className="form-text text-muted">cedula o identificacion</span>
                            </div>
                            <div className="col-md-6 form-row">

<label>Beneficiario:</label>
<div className="input-group">
  
{name}                              </div>
<span className="form-text text-muted">nombre completo</span>
</div>
     
                        </div>

                        <div className="form-group row">
                            <div className="col-md-6 form-row">

                                <label>Moneda:</label>
                                <div className="input-group">
                                    <div className="input-group-prepend"></div>
{type}                              </div>
                                <span className="form-text text-muted">Saldo</span>
                            </div>

                
                         
     
                        </div>

     
                    <div className="modal-footer">
                    <button type="reset" className="btn btn-danger"  ref={this.resetRef}>Resetear</button>
                      {button}
                </div>
                </form>
            </div>
        </div>
                    </div>
                   </div>
              
       );
    }
}


export default FormEditCard;