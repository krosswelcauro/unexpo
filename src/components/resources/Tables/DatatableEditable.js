import React, { Component, Fragment } from 'react'
import {MDBDataTableV5, MDBCard, MDBCardBody, MDBCardHeader, MDBInput, MDBBtn, MDBTable, MDBTableBody, MDBTableHead ,MDBTableEditable  } from 'mdbreact';
import "@fortawesome/fontawesome-free/css/all.min.css";
import "bootstrap-css-only/css/bootstrap.min.css";
import "mdbreact/dist/css/mdb.css";
import config from './../../../services/config'
import Spin from './../../Spinners';
import { Assing } from '../../../services/resource';
import {Link,Redirect} from 'react-router-dom'
import {destroyAll,restore} from './../../../services/resource';

    class DataTableEditable extends Component {
      constructor(props) {
        super(props);
        // No llames this.setState() aquí!
        this.state = {
         
          
         }
       
        }
      
      
      

     
     componentDidUpdate(nextProps, prevState) {
        
    
      
        
        }
     
      
      
         columns = ["Person Name", "Age", "Company Name", "Country", "City"];

         data = [
          ["Aurelia Vega", 30, "Deepends", "Spain", "Madrid"],
          ["Guerra Cortez", 45, "Insectus", "USA", "San Francisco"],
          ["Guadalupe House", 26, "Isotronic", "Germany", "Frankfurt am Main"],
          ["Elisa Gallagher", 31, "Portica", "United Kingdom", "London"]
        ];

     
       
        render() { 
      
        
     
            return (
               
         <Fragment>




                <MDBCard narrow>
                <MDBCardHeader className="view view-cascade gradient-card-header blue-gradient d-flex justify-content-between align-items-center py-2 mx-4 mb-3">
<div>
<a href="#" className="white-text mx-3">Notas</a>
</div>

                </MDBCardHeader>
                <MDBCardBody cascade>
        
                <MDBTableEditable  data={this.data} columns={this.columns}  striped bordered style={{background:"rgb(220,220,220)" }}/>


                </MDBCardBody>
              </MDBCard>
              <div>
           
              </div>

              </Fragment>
                );
        }
    }
     
    export default DataTableEditable;
