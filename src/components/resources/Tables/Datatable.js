import React, { Component, Fragment } from 'react'
import {MDBDataTableV5, MDBCard, MDBCardBody, MDBCardHeader, MDBInput, MDBBtn, MDBTable, MDBTableBody, MDBTableHead,MDBTableEditable  } from 'mdbreact';
import "@fortawesome/fontawesome-free/css/all.min.css";
import "bootstrap-css-only/css/bootstrap.min.css";
import "mdbreact/dist/css/mdb.css";
import config from './../../../services/config'
import Spin from './../../Spinners';
import { Assing } from '../../../services/resource';
import {Link,Redirect} from 'react-router-dom'
import {destroyAll,restore} from './../../../services/resource';

    class DataTable extends Component {
      constructor(props) {
        super(props);
        // No llames this.setState() aquí!
        this.state = {
          data :{columns:this.props.head,rows:[],
          
         },
         avaliable:false,
         checkbox1:[],
         setCheckbox1:[],
         checkload:false,
         deleteCheckBox:new Array(),
        }
      }
      
      

       showLogs2 = (e) => {
        this.state.setCheckbox1(e);
      };
     componentDidUpdate(nextProps, prevState) {
        
    
      
        
        }
     
       componentDidMount(){
    
        this.setState({checkload:false})


           this.props.callback()
       }
      
     

   
       
       ChangeParam = async() =>{
        this.setState({avaliable:!this.state.avaliable})
       await  this.props.reload(this.state.avaliable)
       this.setState({checkload:false})
         }
    
    rows=  [
    
      {
        label: 'Office',
        field: 'office',
        sort: 'asc',
        width: 200
        }
    ]
 
     spin = [
        {
          'dni': 1,
          'nombre':<Spin></Spin>,
          'segundo_nombre': <Spin></Spin>,
          'apellido': <Spin></Spin>,
          'segundo_apellido':<Spin></Spin>
        },
        
      ];
      
      PushPop = (e,val)=>{ 
    console.log(e.target.checked,val)
    let array = this.state.deleteCheckBox;
    if(e.target.checked){
     array.push(val)
     this.setState({deteleCheckBox:array})
    }else{
      //array = array.filter((i) => i !== 3);
      let i = array.indexOf(val); // obtenemos el indice
      array.splice(i, 1)
      this.setState({deteleCheckBox:array})
    }
    
    console.log(this.state.deleteCheckBox)
    

  
    //  this.state.deleteCheckBox.push(param);

      //this.state.deleteCheckBox.pop(param);
      }

      DeleteAll = async() =>{
      if (this.state.deleteCheckBox.length > 0) {
        destroyAll(this.props.onDelete,localStorage.getItem("token"),{array:this.state.deleteCheckBox},(respuesta)=>{
          this.setState({checkload:false})
          this.props.callback()
       console.log(respuesta)
      })
        
      }else{
        console.log("Esta vacio, no hay mounstruos aqui")
      }
      }

      Restore = (id) =>{
        
        restore(this.props.onRestore + id,localStorage.getItem("token"),(respuesta)=>{
          this.setState({checkload:false})
          this.props.callback()
       console.log(respuesta)
      })
      }
       Assing = async ()=>{
   
        if(this.state.checkload==false){
          let array =this.props.data.data.map((value,key)=>{
            let obj  = (value.deleted_at == null)?{"check":<Fragment>                          
            <label class="switch" data-toggle= "tooltip" data-placement= "right" title= "Estado de inscripcion"> 
              <input className="checkbox" type="checkbox"  onChange={(e)=>this.PushPop(e,value.id)} id="check"/>
              <span className="slider round"></span>
            
            </label></Fragment>
            
          }:{"check":    <i className="material-icons mt-0"> query_builder</i>}
            let obj2  =(value.deleted_at == null)?
            {"opciones": <Link to={`/${this.props.optionTitle}/actualizar/${value.id}`} class="btn btn-info btn-raised btn-xs" onClick={()=>this.setState({checkload:false})}><i class="fas fa-edit mt-0"></i><div class="ripple-container"></div></Link>}
            :{"opciones": <button type ="button" class="btn btn-ligth btn-raised btn-xs" onClick={()=>this.Restore(value.id)}><i class="fas fa-history mt-0"></i><div class="ripple-container"></div></button>}

            Object.assign(value,obj);
             Object.assign(value,obj2);

             
             return value
             
            // value.assign("hola")
             //this.state.rows.push(value)
            })
            this.setState({data:{columns:this.props.head,rows:array}})
            this.setState({checkload:true})
        }
      
       }
       
        render() { 
      
         if((this.props.data!=false)){
           this.Assing()
         }
       this.state.data= (this.props.data!=false)?
       
       {columns:this.props.head,rows:this.props.data.data}
       :this.spin
       
            return (
               
         <Fragment>




                <MDBCard narrow>
                <MDBCardHeader className="view view-cascade gradient-card-header blue-gradient d-flex justify-content-between align-items-center py-2 mx-4 mb-3">
                  <div>
                    <MDBBtn outline rounded size="sm" color="white" className="px-2">
                     
                      <i className="material-icons mt-0"> home</i>
                    </MDBBtn>
                    <MDBBtn outline rounded size="sm" color="white" className="px-2" onClick={()=>this.ChangeParam()}>
                     
                      <i className="material-icons mt-0"> history</i>
                    </MDBBtn>
                  </div>
                  <a href="#" className="white-text mx-3">{this.props.title}</a>
                  <div>
                  
                    <MDBBtn outline rounded size="sm" color="white" className="px-2" onClick={()=>this.DeleteAll()}>
                      <i className="material-icons mt-0"> delete</i>
                    </MDBBtn>
                    <Link to ={{pathname:this.props.onCreate}}> <i className="material-icons mt-0 border boder-white bg-white">add_circle_outline</i></Link>
               

   
                  </div>
                </MDBCardHeader>
                <MDBCardBody cascade>
        
                  <MDBDataTableV5 
                  
                  responsive
                   btn fixed
                   overflow-y 
                  hover entriesOptions={[5, 20, 25]} entries={5} pagesAmount={4} data ={ this.state.data} fullPagination >
                  
                  </MDBDataTableV5>

                </MDBCardBody>
              </MDBCard>
              <div>
           
              </div>

              </Fragment>
                );
        }
    }
     
    export default DataTable;
