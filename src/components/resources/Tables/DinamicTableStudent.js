import React, { Component, Fragment } from 'react';
//import Pagination from './Pagination';
import routes from './../../../services/routes';
import RowCard from './RowCard';
//import Swal from 'sweetalert2';
import {Link} from 'react-router-dom';
import { Confirm, m_toast } from './../sweetalert';
import Spinner from './../../Spinners';
import { getAll,findOrChange,Save} from '../../../services/general';
import { destroy} from '../../../services/resource';

class DinamicTableStudent extends Component {
    state = {
        cardOk: false,
        card: [],
        cardCopy: [],
        filterTable: 1,
        filterTable2: 1,
        findTable: "",
        cadEdit:"",
        show:false,
        optionTitle:"vacio",
        avaliable:false
    }

    filterValue = React.createRef();
    filterValue2 = React.createRef();
    findValue = React.createRef();

    componentDidMount() {
        this.setState({ optionTitle:  this.props.optionTitle})
        this.setState({ filterTable:  this.props.defaultFilter})
        this.setState({ filterTable2: this.props.defaultFilter2 })

        console.log(this.props.data)
       
       this.props.callback()
    }

    

    changeFilter = (e) => {
        let properties = new Array();
        this.props.filters.map((value,key)=>{
            properties.push(value)
        })
      
        if (typeof this.props.defaultFilter != undefined) {
            switch (this.filterValue.current.value) {
                case properties[0].title:
                    this.props.Change(properties[0].title)

                    break;
                case properties[1].title:
                      this.props.Change(properties[1].title)
                default:
                    break;
            }
        }
    }
    FindUser = async(find) =>{
      
       find = find.id
       
        let token =  localStorage.getItem('token');
        let users =  await findOrChange('GET',routes.findOrChange, token, find,respuesta => {
            if (respuesta instanceof Error) {
                console.log(respuesta)
            } else {
                console.log(respuesta)
            this.setState({findOne:respuesta})
            this.setState({show:true})
        
            }
        }).then()
    
    }
    changeFilter2 = (e) => {
      this.props.Change2(e.target.value)
    }

    Search = (e) => {

   
    let find = e.target.value;
      if(find.length>3){
        let items = [...this.state.card];
        items.find(item => {
               return item.code ==="test01@gmail.com"
     
        })
        let cards = this.state.card;
        cards.items = items;       
        console.log(items)
        this.setState({ cardCopy: cards });
     
     
        this.setState({findTable:find})
      }else{
     
        this.setState({findTable:""})
      }
        /* if (this.findValue != "") {
            let items = [...this.state.user];
            items.filter(item => {
                if (item.email == this.findValue.current.value) {
                    console.log(item.email,this.findValue.current.value)

                    return item
                }
            })
            let users = this.state.user;
            users.items = items;       
            console.log(users.items)
            this.setState({ user: users });
            console.log(this.state.user)
         
        }*/
    }



/*     CheckStatus = async (status) => {
        let s = status;
        Swal.fire({
            title: "Advertencia",
            text: "¿Esta seguro de activar la cuenta?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Confirmar',
            cancelButtonText: "Cancelar"
        }).then(async (result) => {
            if (result.value) {
                await this.Status(s)
                Swal.fire(
                    "Cambios realizados",
                    "Estado cambiado a verficado",
                    'success'
                );
            }
        })
    }

  

   Delete = async (status) => {
        console.log(status)
        let s = status;
        Swal.fire({
            title: "Advertencia",
            text: "¿Esta seguro de eliminar la tarjeta?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Confirmar',
            cancelButtonText: "Cancelar"
        }).then(async (result) => {
            if (result.value) {
                await this.Status(s,"DELETED");
                this.props.Load2(status.id,"DELETED")
                Swal.fire(
                    "Cambios realizados",
                    "Tarjeta borrada",
                    'success'
                );
            }
        })
    }
*/

Delete = async (val) => {
    
    destroy("alumno",localStorage.getItem("token"),val.id,(respuesta)=>{
        this.props.callback()
     console.log(respuesta)
    })
 }

 ChangeParam = () =>{
    this.setState({avaliable:!this.state.avaliable})
    this.props.reload(this.state.avaliable)
     }

Load = async (e,code) => {
    let items = [...this.state.card];
    items.filter(item => {
        if (item.id == e) {
            
            return item.code = code
        }
    })
    let cards = this.state.card;
    cards.items = items;
    console.log(cards)
    this.setState({ card: cards })
    this.setState({ cardCopy: cards })
    }



    Status = async (status,param) => {
        await findOrChange('DELETE',routes.routeTheCard, localStorage.getItem('token'), status.code,
            respuesta => {
                console.log(respuesta)
               
                if (respuesta instanceof Error) {
                    m_toast('error',respuesta)

                } else {
                    m_toast('success',respuesta)

                }
            })
    }

    Edit = (value) =>{
        
        console.log(value.id)
        this.setState({cadEdit:value.id})
    }

    render() {
      let  optionTitle = this.props.optionTitle;
   let busqueda = this.state.findTable;
   if(busqueda!=""){
   // Mejor le digo a jose que haga una api jaja
   }
 
   let body2 ;
 if(this.props.data.items !=false){
    
   body2 = <Fragment>
         {console.log(this.props.data.items.data),this.props.data.items.data.map((value, key) => {
   let actualizar = (value.deleted_at===null)? <td><Link to={`/${this.props.optionTitle}/actualizar/${value.id}`} class="btn btn-success btn-raised btn-xs"><i class="zmdi zmdi-refresh"></i><div class="ripple-container"></div></Link></td>:<td>no dispnible</td>
   let eliminar =  (value.deleted_at===null)? <td><button type="button" onClick={()=>this.Delete(value)} href="#!" class="btn btn-danger btn-raised btn-xs"><i class="zmdi zmdi-delete"></i><div class="ripple-container"></div></button></td>:<td>no dispnible</td>
  
        return(
                 <Fragment>
                     <tr>
                        
                        <td> {value.dni} </td>
                        <td> {value.nombre} </td>                       
                        <td> {value.segundo_nombre} </td>
                        <td> {value.apellido} </td>
                        <td> {value.segundo_apellido} </td>
                        {actualizar}
                    {eliminar}
                     </tr>
                 </Fragment>
             )
        })}
 </Fragment>

 }else{
    body2= "cargando...";
 }

    let content = (this.props.data.items !=false)?<Fragment>
<label>Disponibilidad</label>                            
<label class="switch" data-toggle= "tooltip" data-placement= "right" title= "Estado de inscripcion"> 
  <input type="checkbox" defaultChecked="defaultChecked" onChange={()=>this.ChangeParam()} id="check"/>
  <span class="slider round"></span>

</label>
							<div className="table-responsive">
								<table className="table table-hover text-center">
									<thead>
										<tr>
                                        {this.props.cabeceras.map((value, key) => {
                        return (
                            <th  data-field={value.title} className=""><span>{value.title}</span></th>
                        );
                    }
                    )}
         
                    <th data-field="actualizar" className=""><span>Actualizar</span></th>
                    <th data-field="eliminar" className=""><span>Eliminar</span></th>

                    
										</tr>
									</thead>
									<tbody>
                                    {body2}
										
									</tbody>
								</table>
								
							</div>
				
    
    </Fragment>:<Spinner></Spinner>

        return (
        <div className="container-fluid">

            {content}
     
        </div>
        )
    
    }
}

export default DinamicTableStudent;