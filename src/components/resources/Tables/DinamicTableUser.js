import React, { Component, Fragment } from 'react';
//import Pagination from './Pagination';
import routes from '../../../services/routes';
import { getAllUsers, changeStatus,findUser } from '../../../services/user';
import Row from './Row';
import Swal from 'sweetalert2';
import {Link} from 'react-router-dom';
import { Confirm } from '../sweetalert';
import ModalUsers from '../Modal/ModalUsers';
import ModalShowUsers from '../Modal/ModalShowUser';
import Spinner from '../../MiniSpin';
import Page from '../../Pagination';
class DinamicTableUser extends Component {
    state = {
        userOk: false,
        user: [],
        userCopy: [],
        filterTable: 1,
        filterTable2: 1,
        findTable: "",
        userEdit:"",
        show:false,
        optionTitle:"tio"
    }

    filterValue = React.createRef();
    filterValue2 = React.createRef();
    findValue = React.createRef();

    componentDidMount() {
        this.setState({ optionTitle:  this.props.optionTitle})
        this.setState({ filterTable:  this.props.defaultFilter})
        this.setState({ filterTable2: this.props.defaultFilter2 })
        
        this.props.callback()
     /*
        let users =  getAllUsers(routes.routeAllUsers, token, respuesta => {
            if (respuesta instanceof Error) {
            } else {
            }
        }).then(e =>{
      
            this.setState({ userOk: true })
            this.setState({ user:[...this.props.data ]  })
            this.setState({ userCopy:[...this.props.data ]  })
            if (typeof this.props.defaultFilter != undefined) {
                switch (this.props.defaultFilter) {
                    case "Empresa":
                        this.setState({ filterTable: 3 })
                        break;
                    case "Cliente":
                        this.setState({ filterTable: 2 })
                        break;

                    default:
                        break;
                }
            }
        })
     */
    }

    changeFilter = (e) => {
        let properties = new Array();
        this.props.filters.map((value,key)=>{
            properties.push(value)
        })
      
        if (typeof this.props.defaultFilter != undefined) {
            switch (this.filterValue.current.value) {
                case properties[0].title:
                    this.setState({ filterTable: 3 })
                    break;
                case properties[1].title:
                    this.setState({ filterTable: 2 })
                    break;
                default:
                    break;
            }
        }
    }
    FindUser = async(find) =>{
      
       find = find.id
       
        let token =  localStorage.getItem('token');
        let users =  await findUser(routes.routeFindUser, token, find,respuesta => {
            if (respuesta instanceof Error) {
                console.log(respuesta)
            } else {
                console.log(respuesta)
            this.setState({findOne:respuesta})
            this.setState({show:true})
        
            }
        }).then()
    
    }
    changeFilter2 = () => {
        let properties = new Array();
        this.props.filters2.map((value,key)=>{
            properties.push(value)
        })
        console.log(properties)
        if (typeof this.props.defaultFilter2 != undefined) {
            switch (this.filterValue2.current.value) {
                case properties[0].title:
                    this.setState({ filterTable2: "VERIFIED_ACCOUNT" })
                    break;
                case properties[1].title:
                    this.setState({ filterTable2: "ACCOUNT_NOT_VERIFIED" })

                    break;
                case properties[2].title:
                    this.setState({ filterTable2: "DELETED" })
                    break;
                case properties[3].title:
                    this.setState({ filterTable2: "DISABLED" })
                    break;
                default:
                    break;
            }
        }
    }

    Search = (e) => {

   
    let find = e.target.value;
      if(find.length>3){
        let items = [...this.state.user];
        items.find(item => {
               return item.email ==="test01@gmail.com"
     
        })
        let users = this.state.user;
        users.items = items;       
        console.log(items)
        this.setState({ userCopy: users });
     
     
        this.setState({findTable:find})
      }else{
     
        this.setState({findTable:""})
      }
        /* if (this.findValue != "") {
            let items = [...this.state.user];
            items.filter(item => {
                if (item.email == this.findValue.current.value) {
                    console.log(item.email,this.findValue.current.value)

                    return item
                }
            })
            let users = this.state.user;
            users.items = items;       
            console.log(users.items)
            this.setState({ user: users });
            console.log(this.state.user)
         
        }*/
    }



    CheckStatus = async (status) => {
        let s = status;
        Swal.fire({
            title: "Advertencia",
            text: "¿Esta seguro de activar la cuenta?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Confirmar',
            cancelButtonText: "Cancelar"
        }).then(async (result) => {
            if (result.value) {
                await this.Status(s)
                Swal.fire(
                    "Cambios realizados",
                    "Estado cambiado a verficado",
                    'success'
                );
            }
        })
    }

    Desactivate = async (status,param) => {
        console.log(status)
        let s = status;
        Swal.fire({
            title: "Advertencia",
            text: "¿Esta seguro de desactivar la cuenta?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Confirmar',
            cancelButtonText: "Cancelar"
        }).then(async (result) => {
            if (result.value) {
                await this.Status(s,param);
                this.props.Load2(status.id,param)
                Swal.fire(
                    "Cambios realizados",
                    "Cuenta desactivada",
                    'success'
                );
            }
        })
    }

    Delete = async (status,param) => {
        console.log(status)
        let s = status;
        Swal.fire({
            title: "Advertencia",
            text: "¿Esta seguro de eliminar la cuenta?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Confirmar',
            cancelButtonText: "Cancelar"
        }).then(async (result) => {
            if (result.value) {
                await this.Status(s,param);
                this.props.Load2(status.id,param)
                Swal.fire(
                    "Cambios realizados",
                    "Cuenta borrada",
                    'success'
                );
            }
        })
    }
  componentWillReceiveProps(){
      return (
<h1>Cambios..</h1>
      );
  }
    CheckStatus = async (status) => {
        let s = status;
        Swal.fire({
            title: "Advertencia",
            text: "¿Esta seguro de activar la cuenta?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Confirmar',
            cancelButtonText: "Cancelar"
        }).then(async (result) => {
            if (result.value) {
                await this.Status(s,"VERIFIED_ACCOUNT")
                Swal.fire(
                    "Cambios realizados",
                    "Estado cambiado a verficado",
                    'success'
                );
            }
        })
    }
Load = async (e,email) => {
    let items = [...this.state.user];
    items.filter(item => {
        if (item.id == e) {
            
            return item.email = email
        }
    })
    let users = this.state.user;
    users.items = items;
    console.log(users)
    this.setState({ user: users })
    this.setState({ userCopy: users })
    }

    

    Status = async (status,param) => {
        await changeStatus(routes.routeStatusUser,  localStorage.getItem('token'), status.id,{ "status": param },
            respuesta => {
                if (respuesta.status === param) {
                   this.props.StatusUser(status,param)

                }
                if (respuesta instanceof Error) {

                } else {

                }
            })
    }

    Edit = (value) =>{
        
        console.log(value.id)
        this.setState({userEdit:value.id})
    }

    render() {
      let  optionTitle = this.props.optionTitle;
   let busqueda = this.state.findTable;
   if(busqueda!=""){
   // Mejor le digo a jose que haga una api jaja
   }
   let body;
   if(this.props.data.items != false){
    body = <tbody  className=" ">
    {  console.log(this.props.data.items),this.props.data.items.map((value, key) => {
                if (this.state.filterTable === value.role.id) {
                    if (this.state.filterTable2 === value.status.name) {
                        let keys = Object.keys(value);
                        return (
                            <Fragment>
                                <tr className="">
                                
                                    <Row keys={keys} data={value}  callback={this.Load2}> </Row>
                                    
                                    <td className="">
  
                                        <button type="button" className="btn btn-sm " value={value.status.name} title="Verificar cuenta" onClick={() => this.CheckStatus(value)}><i className="text-primary flaticon-user-ok" ></i></button>
  
                                        <Link to={`/${this.props.optionTitle}/edit/${value.id}`} title="Editar Credenciales"  value={value.id} className=" btn btn-sm"  onClick={()=>this.Edit(value)}>
                                            <i className="text-success la la-edit "> </i>
                                        </Link>
                                        <Link to={`/${this.props.optionTitle}/details/${value.id}`} title="Ver detalles"  className="btn btn-sm"   id="show">
                                            <i className=" text-info flaticon-users"></i>
                                        </Link>
  
                                        <button type="button" title="Bannear"  className="btn btn-sm" onClick={()=>this.Desactivate(value,"DISABLED")}>
                                            <i className=" text-secondary fas fa-power-off"></i>
                                        
                                        </button>
                                        <button type="button" title="Eliminar"  className="btn btn-sm" onClick={()=>this.Delete(value,"DELETED")}>
                                            <i className=" text-danger la la-trash"></i>
                                        
                                        </button>
                                       
                                    </td>
                                </tr>
                            </Fragment>
                        );
                    
                    }
                }
            }
        )
    }
  </tbody>

   }else{
       body = <div style={{textAlign:"center",alignContent:"center"}}>
           <Spinner ></Spinner>
           <span>cargando contenido..</span>
       </div>
   }
 
    let content = <Fragment>
         <div className="kt-portlet__body">
             <div className="col-md-3 offset-md-9 form-group"><Link to={`/${this.props.optionTitle}/create`} className="btn btn-ttc bg-ligth border-secondary" type="button"><i className="flaticon-user-add"></i>Agregar</Link></div>
        <div className="kt-form kt-fork--label-right kt-margin-t-20 kt-margin-b-10">
            <div className="row align-items-center">
                <div className="col-xl-8 order-2 order-xl-1">
                    <div className="row align-items-center">
                        <div className="col-md-4 kt-margin-b-20-tablet-and-mobile">
                            <div className="kt-input-icon kt-input-icon--left">
                                <input type="text" className="form-control" placeholder="buscar..." id="generalSearch" ref={this.findValue} onKeyDown={this.Search} />
                                <span className="kt-input-icon__icon kt-input-icon__icon--left">
                                    <span><i className="la la-search"></i></span>
                                </span>
                            </div>
                        </div>

                        <div className="form-inline col-md-4">
                            <div className="">
                                <label>Tipo:</label>
                            </div>
                            <select className="form-control" ref={this.filterValue} defaultValue={ this.props.filters} onChange={this.changeFilter.bind(this.value)}>
                                {
                                    this.props.filters.map((value, key) => {
                                        return (<Fragment>
                                            <option value={value.title}>{value.title}</option>
                                        </Fragment>
                                        );
                                    })
                                }

                            </select>
                        </div>

                        <div className="form-inline col-md-4">
                            <div className="">
                                <label>Estado:</label>
                            </div>
                            <select className="form-control" ref={this.filterValue2} onChange={this.changeFilter2.bind(this.value)} >
                                {
                                    this.props.filters2.map((value, key) => {
                                        return (<Fragment>
                                            <option value={value.title}>{value.show}</option>
                                        </Fragment>
                                        );
                                    })
                                }
                            </select>
                        </div>

                  
                    </div>
                </div>
        
            </div>
        </div>

         {/* Table*/}
    <div id="kt_datatable" className=".kt_datatable">
    <div className=" table" id="scrolling_vertical">
        {/* Inicio*/}
        <table className="table table-striped">
            <thead className="text-left">
                <tr className="nav-i">
                    {this.props.cabeceras.map((value, key) => {
                        return (
                            <th  data-field={value.title} className=""><span>{value.title}</span></th>
                        );
                    }
                    )}
         
                    <th data-field="opciones" className=""><span>Opciones</span></th>
                </tr>
            </thead>

            {/* Tbody*/}
           {body}
            {/* Tbody*/}
        </table>
        <Page></Page>
        {/*Fin */}
    </div>
    </div>
    </div>

   

    
    </Fragment>

        return (<div className="container-fluid">
            {content}
       

        </div>);
    }
}

export default DinamicTableUser;