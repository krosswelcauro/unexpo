import React,{Component} from 'react';
import { BrowserRouter } from 'react-router-dom';
import Router from './Router'
import './App.css';
import './Spinner.css';
import './Switch.css';
class App extends Component {
  state = {  }
  render() { 
    return ( 
      
      <div className="App">
      <BrowserRouter>
        <Router></Router>
      </BrowserRouter>
      </div>
      
      
      
    );
  }
}
 
export default App;