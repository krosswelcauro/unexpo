import React, {Fragment, Component } from 'react';
import { Route } from 'react-router-dom';
import Home from './Home';
import Student from './resources/Form/StudentCreate';
import Profesor from './resources/Form/ProfesorCreate';
import Subject from './resources/Form/SubjectCreate';
import Career from './resources/Form/CareerCreate';
import Secction from './resources/Form/SecctionCreate';

import SubjectUpdt from './resources/Form/SubjectUpdate';
import StudentUpdt from './resources/Form/StudentUpdate';
import ProfesorUpdt from './resources/Form/ProfesorUpdate';
import CareerUpdt from './resources/Form/CareerUpdate';
import SecctionUpdt from './resources/Form/SecctionUpdate';
import Datatable from './resources/Tables/Datatable';
import DatatableEditable from './resources/Tables/DatatableEditable';
import Datatable2 from './resources/Tables/TestDataTable';
import Link from 'react-router-dom';

import { index,indexWith } from './../services/resource';

//import DetailsTill from './resources/Form/FormCard';
//import EditTill from './resources/Form/FormEditCard';
import TableSecctions from './resources/Tables/DinamicTableSecction';
import TableStudent from './resources/Tables/DinamicTableStudent';
import TableProfesor from './resources/Tables/DinamicTableProfesor';
import TableSubject from './resources/Tables/DinamicTableSubject';
import TableCareer from './resources/Tables/DinamicTableCareer';


class SubRoutes extends Component {
    constructor() {
        super()
        this.state = {
            secctions:{items:false},
            subjects:{items:false},
            subjects2:{items:false,ok:false},
            secctions:{items:false},
            profesors:{items:false},
            students:{items:false},
            career:{items:false},
            token: localStorage.getItem('token'),
            filterTable:"",
            defaultFilter:1,
            filterTableSecctions:1,
            filterTableSecctions2:"VERIFIED_ACCOUNT",
            filterTableProfesors:"ASC",
            filterTableProfesors2:"ACTIVE",
            filterTableStudents:"ASC",
            filterTableStudents2:"active"
        }
      
       }
   
       Load2 = async (e,status) => {
        let items = this.state.Profesors;
        items.items.filter(item => {
            if (item.id == e) {
                
                return item.status.name = status
            }
        })
       
        this.setState({ Profesors: items })
      

        }

        LoadSecctions = async (e,status) => {
            let items = [...this.state.card];
           
            items.filter(item => {
                    return item.id != e
                
            })
            let Secctions = this.state.card;
            Secctions.items = items;
            console.log(Secctions)
            this.setState({ card: Secctions })
            this.setState({ cardCopy: Secctions })
    
            }

            LoadAllSecctions = async (data) => {
              let items = this.state.Secctions;
              console.log(items)
                items.items.push(data)
                this.setState({ card: items })
               
        
                }
            ChangeFiltersSecctions = ()=>{
                let items = this.state.secctions;
                items.items.reverse()
                this.setState({secctions:items})
                }

            ChangeFiltersSecctions2 = async (status)=>{
                this.setState({filterTableProfesors2:status})
                await this.reloadSecctions(status)
               
               
                }

          
/*
Ejemplo de Status

        StatusUser = (status,param) =>{
            let items = this.state.Profesors;
            items.items.filter(item => {
                if (item.id == status.id) {
                    return item.status.name = param
                }
            })
            console.log(status,param)
          
            this.setState({ Profesors: items })
          
           }

*/
           EditSecction = (body) =>{
            let items = this.state.secctions;
            items.items.filter(item => {
                if (item.id == body.id) {
                    return body
                }
            }) 

            this.setState({ secctions: items })

           }

    getGroup = async() =>{
    await index("seccion",localStorage.getItem('token'),(respuesta)=>{
        respuesta.data.filter((value,key)=>{
            value.asignatura_id = value.asignatura.nombre
            value.profesor_id = value.profesor.nombre
           // value.carrera_id = value.carrera.nombre
        })
       this.setState({secctions:{items:respuesta}})

    console.log(respuesta)
     })
     

            }

        getCareer = async() =>{
                await index("carrera",localStorage.getItem('token'),(respuesta)=>{
                   this.setState({career:{items:respuesta}})
            
                console.log(respuesta)
                 })
                 
            
                        }
            
 reloadSubject = async(param) =>{
    let route;
    if(param == 1){
        route = "asignatura"
     }else{
        route = "asignatura_no_disponible"
     }
                await index(route,localStorage.getItem('token'),(respuesta)=>{
                   this.setState({subjects:{items:respuesta}})
            
                console.log(respuesta)
                 })
                 
            
                        }

    reloadStudent = async(param) =>{
                            let route;
                            if(param == 1){
                                route = "alumno"
                             }else{
                                route = "mostrar_alumnos_borrados"
                             }
                                        await index(route,localStorage.getItem('token'),(respuesta)=>{
                                           this.setState({students:{items:respuesta}})
                                    
                                        console.log(respuesta)
                                         })
                                         
                                    
                                     }

reloadProfesor = async(param) =>{
                                        let route;
                                        if(param == 1){
                                            route = "profesor"
                                         }else{
                                            route = "mostrar_profesores_borrados"
                                         }
                                                    await index(route,localStorage.getItem('token'),(respuesta)=>{
                                                       this.setState({profesors:{items:respuesta}})
                                                
                                                    console.log(respuesta)
                                                     })
                                                     
                                                
                                    }
                                    reloadCareer = async(param) =>{
                                        let route;
                                        if(param == 1){
                                            route = "carrera"
                                         }else{
                                            route = "mostrar_carreras_borradas"
                                         }
                                                    await index(route,localStorage.getItem('token'),(respuesta)=>{
                                                       this.setState({career:{items:respuesta}})
                                                
                                                    console.log(respuesta)
                                                     })
                                                     
                                                
                                    }

                                    reloadSecction= async(param) =>{
                                        let route;
                                        if(param == 1){
                                            route = "seccion"
                                         }else{
                                            route = "seccion_no_disponible"
                                         }
                                                    await index(route,localStorage.getItem('token'),(respuesta)=>{
                                                       this.setState({secctions:{items:respuesta}})
                                                
                                                    console.log(respuesta)
                                                     })
                                                     
                                                
                                    }
                                    
    getStudent= async() =>{
                await index("alumno",localStorage.getItem('token'),(respuesta)=>{
                   this.setState({students:{items:respuesta}});

                   //
            
        
                   //
                console.log(respuesta)
                 })
        
                        }

                        getProfesor= async() =>{
                            await index("profesor",localStorage.getItem('token'),(respuesta)=>{
                               this.setState({profesors:{items:respuesta}})
                        
                            console.log(respuesta)
                             })
                        
                                    }

     getSubject= async() =>{
                            await index("asignatura",localStorage.getItem('token'),(respuesta)=>{
                               this.setState({subjects:{items:respuesta}})
                        
                            console.log(respuesta)
                             })
                        
                                    }

                                    getAll= async() =>{
                                        await index("asignatura",localStorage.getItem('token'),(respuesta)=>{
                                           this.setState({subjects2:{items:respuesta,ok:true}})
                                    
                                        console.log(3,respuesta)
                                         })
                                         await index("profesor",localStorage.getItem('token'),(respuesta)=>{
                                            this.setState({profesors:{items:respuesta}})
                                     
                                         console.log(3,respuesta)
                                          })
                                          await index("carrera",localStorage.getItem('token'),(respuesta)=>{
                                            this.setState({career:{items:respuesta}})
                                     
                                         console.log(3,respuesta)
                                          })
                                    
                                                }
            reloadSecctions = (status) =>{
           /*
                let Secctions =  getAll(`card?status=${status}&limit=`,this.state.token, respuesta => {
                    if (respuesta instanceof Error) {
                      
        
                    } else {
                        
                        this.setState({ cardOk: true })
                        this.setState({ secctions:{ ...respuesta }  })
                        console.log(this.state.secctions)
                        switch (this.state.defaultFilter) {
                            case "Empresa":
                                this.setState({ filterTable: 3 })
                                break;
                            case "Cliente":
                                this.setState({ filterTable: 2 })
                                break;
        
                            default:
                                break;
                        }
                    }
                }).then(e =>{
              
                   
                   
                })
        */
              
               }
       getSecctions = () =>{

        /*   
        let Secctions =  getAll(`card?status=${this.state.filterTableProfesors2}&limit=`,this.state.token, respuesta => {
            if (respuesta instanceof Error) {
              

            } else {
                
                this.setState({ cardOk: true })
                this.setState({ secctions:{ ...respuesta }  })
                console.log(this.state.Secctions)
                switch (this.state.defaultFilter) {
                    case "Empresa":
                        this.setState({ filterTable: 3 })
                        break;
                    case "Cliente":
                        this.setState({ filterTable: 2 })
                        break;

                    default:
                        break;
                }
            }
        }).then(e =>{
      
           
           
        })
*/
      
       }

   
       getOneProfesor = ()=>{
       
     /*
        One(routes.routeUser,this.state.token,
            respuesta => {
                if (respuesta instanceof Error) {

                } else {
                    this.setState({ user: { ...respuesta } })
                    this.setState({ userOk: true })
                    console.log(respuesta)
                }
            }
        );
     */
       }
    render() { 
        return (
         <Fragment>
            {/*Rutas Generales*/}
            <Route exact path="/student/create">
                    <Student/>
               </Route>

            <Route exact path="/seccioness" >
            <div className="page-header">
                            <h1 className="text-titles"><i className="zmdi zmdi-font zmdi-hc-fw"></i> Administration <small>Seccion</small></h1>
                          </div>
            <div className="row">
				<div className="col-xs-12">
					<ul className="nav nav-tabs" style={{marginBottom: 15}}>
					  	<li className="active"><a href="#new" data-toggle="tab">Nuevo</a></li>
					  	<li><a href="#list" data-toggle="tab">Lista</a></li>
					</ul>
					<div id="myTabContent" className="tab-content">
						<div className="tab-pane fade active in" id="new">
                            <Secction  callback={this.getGroup}
                               callback2 ={this.getAll}
                              profesor={this.state.profesors}
                              career={this.state.career}
                              subject={this.state.subjects2}
                            />
						</div>
					  	<div className="tab-pane fade" id="list">
							<div className="table-responsive">
							<TableSecctions
                    optionTitle ="secciones"
                    defaultFilter={this.state.filterTableSecctions}
                    defaultFilter2={this.state.filterTableSecctions2}
                    filters={[{ title: "Activas" },{ title: "Borradas" }]}
                    filters2={[ { title: "I", show: "Semestre I" },{ title: "II", show: "Semestre II" },
                { title: "III", show: "Semestre III" },{ title: "IV", show: "Semestre IV" }]}
                    data={this.state.secctions}
                    userOk={this.state.secctionOk}
                    cabeceras={[{ title: "Asignatura" }, { title: "Profesor" },
                    { title: "Alias" }, , { title: "Lapso" }, { title: "Estado" }]}
                    callback ={this.getGroup}
                    reload ={this.reloadSecction}
                  
                
                    Load2 ={this.Load2}
                /> 
							</div>
					  	</div>
					</div>
				</div>
			</div>
            
             </Route>

             <Route exact path="/asignaturas" >
            <div className="page-header">
                            <h1 className="text-titles"><i className="zmdi zmdi-font zmdi-hc-fw"></i> Administration <small>Asignatura</small></h1>
                          </div>
            <div className="row">
				<div className="col-xs-12">
					<ul className="nav nav-tabs" style={{marginBottom: 15}}>
					  	<li className="active"><a href="#new" data-toggle="tab">Nuevo</a></li>
					  	<li><a href="#list" data-toggle="tab">Lista</a></li>
					</ul>
					<div id="myTabContent" className="tab-content">
						<div className="tab-pane fade active in" id="new">
							<Subject  callback={this.getSubject}/>
						</div>
					  	<div className="tab-pane fade" id="list">
							<div className="table-responsive">
							<TableSubject
                    optionTitle ="asignatura"
                    defaultFilter={this.state.filterTableSecctions}
                    defaultFilter2={this.state.filterTableSecctions2}
                    filters={[{ title: "Activas" },{ title: "Borradas" }]}
                    filters2={[ { title: "I", show: "Semestre I" },{ title: "II", show: "Semestre II" },
                { title: "III", show: "Semestre III" },{ title: "IV", show: "Semestre IV" }]}
                    data={this.state.subjects}
                   
                    cabeceras={[{ title: "Nombre" }, { title: "Matricula" },
                    { title: "Disponibilidad" }]}
                    callback ={this.getSubject}
                    reload ={this.reloadSubject}
                
                    Load2 ={this.Load2}
                /> 
							</div>
					  	</div>
					</div>
				</div>
			</div>
            
             </Route>

             <Route exact path="/alumnos" >
            <div className="page-header">
                            <h1 className="text-titles"><i className="zmdi zmdi-face zmdi-hc-fw"></i> Administration <small>Alumno</small></h1>
                          </div>
            <div className="row">
				<div className="col-xs-12">
					<ul className="nav nav-tabs" style={{marginBottom: 15}}>
					  	<li className="active"><a href="#new" data-toggle="tab">Nuevo</a></li>
					  	<li><a href="#list" data-toggle="tab">Lista</a></li>
					</ul>
					<div id="myTabContent" className="tab-content">
						<div className="tab-pane fade active in" id="new">
							<Student callback={this.getStudent}/>
						</div>
					  	<div className="tab-pane fade active in" id="list">
							<div className="table-responsive">
							<TableStudent
                    optionTitle ="alumno"
                    defaultFilter={this.state.filterTableSecctions}
                    defaultFilter2={this.state.filterTableSecctions2}
                    filters={[{ title: "Activas" },{ title: "Borradas" }]}
                    filters2={[ { title: "I", show: "Semestre I" },{ title: "II", show: "Semestre II" },
                { title: "III", show: "Semestre III" },{ title: "IV", show: "Semestre IV" }]}
                    data={this.state.students}
                   
                    cabeceras={[{ title: "Cedula" },{ title: "Nombre" }, { title: "Segundo nombre" },
                    { title: "Apellido" }, { title: "Segundo apellidos" }]}
                    callback ={this.getStudent}
                    reload ={this.reloadStudent}
                
                    Load2 ={this.Load2}
                /> 
							</div>
					  	</div>
					</div>
				</div>
			</div>
            
             </Route>

             <Route exact path="/profesor" >
            <div className="page-header">
                            <h1 className="text-titles"><i className="zmdi zmdi-font zmdi-hc-fw"></i> Administration <small>Profesor</small></h1>
                          </div>
            <div className="row">
				<div className="col-xs-12">
					<ul className="nav nav-tabs" style={{marginBottom: 15}}>
					  	<li className="active"><a href="#new" data-toggle="tab">Nuevo</a></li>
					  	<li><a href="#list" data-toggle="tab">Lista</a></li>
					</ul>
					<div id="myTabContent" className="tab-content">
						<div className="tab-pane fade active in" id="new">
							<Profesor callback={this.getProfesor}/>
						</div>
					  	<div className="tab-pane fade" id="list">
							<div className="table-responsive">
							<TableProfesor
                    optionTitle ="profesor"
                    defaultFilter={this.state.filterTableSecctions}
                    defaultFilter2={this.state.filterTableSecctions2}
                    filters={[{ title: "Activas" },{ title: "Borradas" }]}
                    filters2={[ { title: "I", show: "Semestre I" },{ title: "II", show: "Semestre II" },
                { title: "III", show: "Semestre III" },{ title: "IV", show: "Semestre IV" }]}
                    data={this.state.profesors}
                   
                    cabeceras={[{ title: "Cedula" },{ title: "Nombre" }, { title: "Segundo nombre" },
                    { title: "Apellido" }, { title: "Segundo apellidos" }]}
                    callback ={this.getProfesor}
                    reload ={this.reloadProfesor}
                
                    Load2 ={this.Load2}
                /> 
							</div>
					  	</div>
					</div>
				</div>
			</div>
            
             </Route>

             <Route exact path="/carrera" >
            <div className="page-header">
                            <h1 className="text-titles"><i className="zmdi zmdi-font zmdi-hc-fw"></i> Administration <small>Carrera</small></h1>
                          </div>
            <div className="row">
				<div className="col-xs-12">
					<ul className="nav nav-tabs" style={{marginBottom: 15}}>
					  	<li className="active"><a href="#new" data-toggle="tab">Nuevo</a></li>
					  	<li><a href="#list" data-toggle="tab">Lista</a></li>
					</ul>
					<div id="myTabContent" className="tab-content">
						<div className="tab-pane fade active in" id="new">
							<Career callback={this.getCareer}/>
						</div>
					  	<div className="tab-pane fade" id="list">
							<div className="table-responsive">
							<TableCareer
                    optionTitle ="carrera"
                    defaultFilter={this.state.filterTableSecctions}
                    defaultFilter2={this.state.filterTableSecctions2}
                    filters={[{ title: "Activas" },{ title: "Borradas" }]}
                    filters2={[ { title: "I", show: "Semestre I" },{ title: "II", show: "Semestre II" },
                { title: "III", show: "Semestre III" },{ title: "IV", show: "Semestre IV" }]}
                    data={this.state.career}
                   
                    cabeceras={[{ title: "Nombre" }]}
                    callback ={this.getCareer}
                    reload ={this.reloadCareer}
                
                    Load2 ={this.Load2}
                /> 
							</div>
					  	</div>
					</div>
				</div>
			</div>
            
             </Route>
          
             <Route exact path="/home" component={Home} />
                

            <Route exact path="/secctions" >
                <h3 className="mb-5 mt-5"><i className=""></i> Secciones</h3>
                <TableSecctions
                    optionTitle ="secctions"
                    defaultFilter={this.state.filterTableSecctions}
                    defaultFilter2={this.state.filterTableSecctions2}
                    filters={[{ title: "Activas" },{ title: "Borradas" }]}
                    filters2={[ { title: "I", show: "Semestre I" },{ title: "II", show: "Semestre II" },
                { title: "III", show: "Semestre III" },{ title: "IV", show: "Semestre IV" }]}
                    data={this.state.secctions}
                    userOk={this.state.secctionOk}
                    cabeceras={[{ title: "Materia" }, { title: "Profesor" },
                    { title: "Nombre" }, , { title: "Lapso" }, { title: "Estado" }]}
                    callback ={this.getSecctions}
                
                    Load2 ={this.Load2}
                /> 
            </Route>

            <Route exact path="/alumno" >
<Datatable 
title ="Alumnos" 
optionTitle ="alumno"
onCreate ="/alumno/crear"
reload ={this.reloadStudent }
onDelete ="alumno/eliminar_varios"
onRestore ="alumno/restaurar/"
head ={[
    {
        label: 'Selceccionar',
        field: 'check',
        sort: 'asc',
        width: 150
        },
    {
    label: 'Cedula',
    field: 'dni',
    sort: 'asc',
    width: 150
    },
    {
    label: 'Nombre',
    field: 'nombre',
    sort: 'asc',
    width: 270
    },
    {
    label: 'Segundo Nombre',
    field: 'segundo_nombre',
    sort: 'asc',
    width: 200
    },
    {
        label: 'Apellido',
        field: 'apellido',
        sort: 'asc',
        width: 270
        },
        {
            label: 'Segundo Apellido',
            field: 'segundo_apellido',
            sort: 'asc',
            width: 270
            },
            {
                label: 'Opciones',
                field: 'opciones',
                sort: 'asc',
                width: 270
                }
    ]}
    callback ={this.getStudent}
data = {this.state.students.items} 
></Datatable>
                </Route>
                <Route exact path="/asignatura" >
<Datatable 
title ="Asignaturas" 
optionTitle ="asignatura"
onCreate ="/asignatura/crear"
reload ={this.reloadSubject}
onDelete ="asignatura/eliminar_varios"
onRestore ="asignatura/restaurar/"
head ={[
    {
        label: 'Selceccionar',
        field: 'check',
        sort: 'asc',
        width: 150
        },
    {
    label: 'Nombre',
    field: 'nombre',
    sort: 'asc',
    width: 150
    },
    {
    label: 'Matricula',
    field: 'matricula',
    sort: 'asc',
    width: 270
    },
    {
    label: 'Disponibilidad',
    field: 'disponibilidad',
    sort: 'asc',
    width: 200
    }
    ]}
    callback ={this.getSubject}
data = {this.state.subjects.items} 
></Datatable>
                </Route>

                <Route exact path="/asignatura" >
<Datatable 
title ="Asignaturas" 
optionTitle ="asignatura"
onCreate ="/asignatura/crear"
reload ={this.reloadSubject}
onDelete ="asignatura/eliminar_varios"
onRestore ="asignatura/restaurar/"
head ={[
    {
        label: 'Selceccionar',
        field: 'check',
        sort: 'asc',
        width: 150
        },
    {
    label: 'Nombre',
    field: 'nombre',
    sort: 'asc',
    width: 150
    },
    {
    label: 'Matricula',
    field: 'matricula',
    sort: 'asc',
    width: 270
    },
    {
    label: 'Disponibilidad',
    field: 'disponibilidad',
    sort: 'asc',
    width: 200
    }
    ]}
    callback ={this.getSubject}
data = {this.state.subjects.items} 
></Datatable>
                </Route>

                <Route exact path="/seccion" >
<Datatable 
title ="Secciones" 
optionTitle ="seccion"
onCreate ="/seccion/crear"
reload ={this.reloadSecction}
onDelete ="seccion/eliminar_varios"
onRestore ="seccion/restaurar/"
cabeceras={[{ title: "Asignatura" }, { title: "Profesor" },
{ title: "Alias" }, , { title: "Lapso" }, { title: "Estado" }]}
head ={[
    {
        label: 'Selceccionar',
        field: 'check',
        sort: 'asc',
        width: 150
        },
    
    {label: 'Alias',
    field: 'alias',
    sort: 'asc',
    width: 200
    },
    {
        label: 'Lapso',
        field: 'lapso',
        sort: 'asc',
        width: 200
        },
        {
            label: 'Estado',
            field: 'estado',
            sort: 'asc',
            width: 200
            },
    {

    label: 'Asignatura',
    field: "asignatura_id",
    sort: 'asc',
    width: 150,
   
    },
    {
    label: 'Profesor',
    field: 'profesor_id',
    sort: 'asc',
    width: 270
    },
    {
        label: 'Carrera',
        field: 'carrera_id',
        sort: 'asc',
        width: 270
        },
    
    
    ]}
    callback ={this.getGroup}
data = {this.state.secctions.items} 
></Datatable>
                </Route >
              
               <Route exact path="/nota">
              <DatatableEditable></DatatableEditable>
               </Route>
                {/*Rutas especificas*/}

                 {/*Alumnos*/}
                  {/*Crear*/}
                 <Route exact path="/alumno/crear/" >
                 <Student></Student>
                </Route>
                 {/*Actualizar*/}
                <Route exact path="/alumno/actualizar/:id" render={(props) => 
                {
                    let id = props.location.pathname.replace("/alumno/actualizar/", "")
                        return (
                            <Fragment>
                                <h3 className="mb-5 mt-5">Titulo</h3>
                               <StudentUpdt id ={id} ></StudentUpdt>
                            </Fragment>
                        )        
                }} >

                </Route>
            

                <Route exact path="/secciones/detalle/:id" render={(props) => 
                {
                    let id = props.location.pathname.replace("/secciones/detalle/", "")
                        return (
                            <Fragment>
                                <h3 className="mb-5 mt-5">Titulo</h3>
                                {/* Componente de editar*/}
                            </Fragment>
                        )        
                }} >

                </Route>

                
                <Route exact path="/asignatura/actualizar/:id" render={(props) => 
                {
                    let id = props.location.pathname.replace("/asignatura/actualizar/", "")
                        return (
                            <Fragment>
                                <h3 className="mb-5 mt-5">Titulo</h3>
                               <SubjectUpdt id ={id}></SubjectUpdt>
                            </Fragment>
                        )        
                }} >

                </Route>

               

                <Route exact path="/profesor/actualizar/:id" render={(props) => 
                {
                    let id = props.location.pathname.replace("/profesor/actualizar/", "")
                        return (
                            <Fragment>
                                <h3 className="mb-5 mt-5">Titulo</h3>
                               <ProfesorUpdt id ={id}></ProfesorUpdt>
                            </Fragment>
                        )        
                }} >

                </Route>

           

                <Route exact path="/carrera/actualizar/:id" render={(props) => 
                {
                    let id = props.location.pathname.replace("/carrera/actualizar/", "")
                        return (
                            <Fragment>
                                <h3 className="mb-5 mt-5">Titulo</h3>
                               <CareerUpdt id ={id}></CareerUpdt>
                            </Fragment>
                        )        
                }} >

                </Route>
                <Route exact path="/secciones/actualizar/:id" render={(props) => 
                {
                    let id = props.location.pathname.replace("/secciones/actualizar/", "")
                        return (
                            <Fragment>
                                <h3 className="mb-5 mt-5">Titulo</h3>
                               
                            </Fragment>
                        )        
                }} >

                </Route>
                {/* Mas rutas*/}
            <Route exact path="/home" >
                 
            </Route>


        </Fragment> );
    }
}
 
export default SubRoutes;